package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
//实现打印图形---多态继承案例
    class picture{
        void show(){
            System.out.println("打印图形");
        }
}
class circle extends  picture{
    @Override
    void show() {
        System.out.println("⚪");
    }
}
class snajiao extends  picture{
    @Override
    void show() {
        System.out.println("三角形");
    }
}
public class demoo1 {
    public  static void test(picture p){
        p.show();
    }
        public static void main(String[] args) {
        //test(new circle());
            circle c = new circle();
            test(c);
            test(new snajiao());
    }
}
