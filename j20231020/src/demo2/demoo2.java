package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
//通过调用接口实现多继承关系
interface  Iswimming{
    void  swim();
}
interface Irunning{
    void run();
}
interface Iflying{
    void fly();
}
abstract class Animal{
    private   String name;
    private  int age;
    public  abstract  void eat();

    Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
class Dog extends  Animal implements Iswimming,Irunning{
    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void swim() {
        System.out.println(this.getName()+"正在游泳");
    }

    @Override
    public void run() {
        System.out.println(this.getName()+"正在跑步");
    }

    @Override
    public void eat() {
        System.out.println(this.getName()+"正在吃狗粮");
    }
}
public class demoo2 {
    public  static  void test(Animal a){
        a.eat();
    }
    public static  void test1(Irunning irun){
        irun.run();
    }
    public static  void test2(Iswimming iswim){
        iswim.swim();
    }public static  void test3(Iflying ifly){
        ifly.fly();
    }
    public static void main(String[] args) {
        Dog dog = new Dog("XIAOBAI",15);
        test(dog);
        test1(dog);
        test2(dog);
    }
}
