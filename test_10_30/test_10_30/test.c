#define _CRT_SECURE_NO_WARNINGS 1
//使用函数计算两个复数之积
//#include <stdio.h>
//struct complex multiply(struct complex x, struct complex y)
//{
//    struct complex product;
//    product.real = x.real * y.real - x.imag * y.imag;
//    product.imag = x.real * y.imag + x.imag * y.real;
//    return product;
//
//}
//
//
//struct complex {
//    int real;
//    int imag;
//};
//
//struct complex multiply(struct complex x, struct complex y);
//
//int main()
//{
//    struct complex product, x, y;
//
//    scanf("%d%d%d%d", &x.real, &x.imag, &y.real, &y.imag);
//    product = multiply(x, y);
//    printf("(%d+%di) * (%d+%di) = %d + %di\n",
//        x.real, x.imag, y.real, y.imag, product.real, product.imag);
//
//    return 0;
//}
//
//
//
//按等级统计学生成绩
int set_grade(struct student* p, int n)
{
    int count = 0, i;

    for (i = 0; i < n; i++)
    {
        if ((*p).score < 60)  /* *p需用括号括起来，因为*p.score等同*(p.score) */
        {
            (*p).grade = 'D';
            count++;
        }
        else if ((*p).score >= 60 && (*p).score <= 69)
            (*p).grade = 'C';
        else if ((*p).score >= 70 && (*p).score <= 84)
            (*p).grade = 'B';
        else
            (*p).grade = 'A';

        p++; //最后有个p++ 代表从p0 ——> p1......... 
    }

    return count; // return 只能返回一个值 所以 返回count  代表 set_grede 的值  
}                //其余的操作通过 *p 来完成 
#include <stdio.h>
#define MAXN 10

struct student {
    int num;
    char name[20];
    int score;
    char grade;
};

int set_grade(struct student* p, int n);

int main()
{
    struct student stu[MAXN], * ptr;
    int n, i, count;

    ptr = stu;
    scanf("%d\n", &n);
    for (i = 0; i < n; i++) {
        scanf("%d%s%d", &stu[i].num, stu[i].name, &stu[i].score);
    }
    count = set_grade(ptr, n);
    printf("The count for failed (<60): %d\n", count);
    printf("The grades:\n");
    for (i = 0; i < n; i++)
        printf("%d %s %c\n", stu[i].num, stu[i].name, stu[i].grade);
    return 0;
}

/* 你的代码将被嵌在这里 */