#define _CRT_SECURE_NO_WARNINGS 1
////编程一 ->
// 实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表
//#include<stdio.h>
//void print( )
//{
//	int i,n,j = 0;
//	scanf("%d", &n);
//	for (i = 1; i < n + 1; i++)
//	{
//		for (j = 1; j < i + 1; j++)
//		{
//			printf("%d * %d = %d\t", j, i, i * j);
//			if (i == j)
//			{
//				printf("\n");
//			}
//		}
//	}
//}
//int main()
//{
//	print();
//	return 0;
//}
//编程二 -> 
//实现一个函数来交换两个整数的内容。
//#include<stdio.h>
//void swap(int* a, int* b)
//{
//	int temp;
//	temp = *a;
//	*a = *b;
//	*b = temp;
//}
//int main()
//{
//	int x, y;
//	printf("请输入两个值：");
//	scanf("%d %d", &x, &y);
//	printf("交换前：x=%d,y=%d\n", x, y);
//	swap(&x, &y);
//	printf("交换后：x=%d,y=%d\n", x, y);
//}
//编程三->
//实现函数判断year是不是润年
//#include<stdio.h>
//int judge()
//{
//    int i = 0;
//    scanf("%d", &i);
//    if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0)
//    {
//        return 1;
//    }
//    else return 0;
//}
//int  main()
//{
//    int j=judge();
//    if (j)
//    {
//        printf("是闰年\n");
//    }
//    else printf("不是闰年\n");
//
//}
//编程四->
//实现一个函数，判断一个数是不是素数。
//利用上面实现的函数打印100到200之间的素数。
#include<stdio.h>
void print()
{
	int i = 0;
	int  j = 0;
	for (i = 100; i <= 200; i++)
	{
		for (j = 2; j < i; j++)
		{
			if (i % j == 0)
				break;
		}
		if (j == i)
		{

			printf("%d ", i);
		}
	}
}
int main()
{
	print();
	return 0;
}
