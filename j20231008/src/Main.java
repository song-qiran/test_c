import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 * Date: 2023-10-08
 * Time: 13:38
 */
//使用方法计算传参的前n个数的阶乘的值
public class Main {
    public  static  int fac(int x){
        int ret = 1;
        for (int i = 1; i <= x; i++) {
            ret*=i;
        }
        return  ret;
    }
    public  static  int facsum(int n){
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum+=fac(i);
        }
        return  sum;
    }
    public static void main1(String[] args) {
        int sum = facsum(5);
        System.out.println(sum);
    }
//计算分数的值
// 计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值    。
public static void main2(String[] args) {
        double sum = 0;
        int flag = 1;
        for (int i = 1; i < 101; i++) {
            sum+= (flag*1.0)/i;
            flag*=-1;
        }
    System.out.println(sum);
}
//使用函数求最大值
// 创建方法求两个数的最大值max2，随后再写一个求3个数的最大值的函数max3。
// 要求：在max3这个函数中，调用max2函数，来实现3个数的最大值计算
public static void main3(String[] args) {
    Scanner sc = new Scanner(System.in);
    int a = sc.nextInt();
    int b = sc.nextInt();
    int c = sc.nextInt();
    System.out.println("前两个数字比较的最大值："+max2(a,b));
    System.out.println("三个数字比较的最大值："+max3(a,b,c));
}
public static int max2(int x,int y){
    int max = x>y?x:y;
    return max;
}
public  static  int max3(int x,int y,int z){
    int max3=max2(x,z);
    return max3>y?max3:y;
}

//求n的阶乘
public static void main4(String[] args) {
    Scanner sc = new Scanner(System.in);
    int a=sc.nextInt();
    int ret = 1;
    for (int i = 1; i <=a; i++) {
        ret*=i;
    }
    System.out.println("n的阶乘是"+ret);
}

//求阶乘和
// 求1！+2！+3！+4！+........+n!的和
public static void main5(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        for (int i = 1; i <=n; i++) {
           sum+= fac2(i);
        }
    System.out.println("前"+n+"阶阶乘的和为"+sum);
}
public static int fac2(int n) {
    int ret = 1;
    for (int i = 1; i <= n; i++) {
        ret *= i;
    }
    return ret;
}

//斐波那契数列迭代求法
public static void main6(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    System.out.println(Fibonacci(n));
}
public  static long Fibonacci(int n)
    {
        if (n == 0)
            return 0;
        else if (n == 1)
            return 1;
        long a = 0;
        long b = 1;
        long c = 1;
        while (n - 2!=0)
        {
            a = b;
            b = c;
            c = a + b;
            n--;
        }
        return c;
    }//迭代法


//在同一个类中,分别定义求两个整数的方法 和 三个小数之和的方法。 并执行代码，求出结果
public static void main7(String[] args) {
    int a = 1,b = 2;
    System.out.println(sum(a,b));
    double c = 1.1, d = 2.2,e = 3.3;
    System.out.println(sum(c,d,e));
}
public static int sum(int a,int b){
        return a+b;
}
public static  double sum(double c,double d,double e){
        return c+d+e;
}


//在同一个类中定义多个方法：要求不仅可以求2个整数的最大值，还可以求3个小数的最大值
public static void main(String[] args) {
    int a = 1,b = 2,c = 3;
    System.out.println("前两个数的最大值为 "+max(a,b));
    System.out.println("前三个数的最大值为 "+max(a,b,c));
}
public  static  int max(int a,int b){
        return a>b?a:b;
}
public  static  int max(int a,int b,int c){
        return max(a,b)>c?max(a,b):c;
}

}