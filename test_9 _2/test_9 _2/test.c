#define _CRT_SECURE_NO_WARNINGS 1
//统计每个月兔子的总数
//#include <stdio.h> 
//int main()
//{
//	int n;
//	while(scanf("%d", &n)!=EOF)
//	{
//		int num1 = 1, num2 = 1, ret = 0;
//		for (int i = 2; i < n; i++)
//		{
//			ret = num1 + num2;
//			num1 = num2;
//			num2 = ret;
//		}
//		printf("%d\n", ret);
//	}
//	return 0;
//}

////数列的和
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//    double n,m,sum = 0;
//    double arr[10000] = { 0 };
//    while ((scanf("%lf %lf", &n, &m) != EOF))
//    {
//          sum=0;
//        for (int i = 0; i < m; i++)
//        {
//            arr[0] = n;
//            arr[i + 1] = sqrt(arr[i]);
//            sum += arr[i];
//        }
//        printf("%.2lf\n", sum);
//    }
//
//    return 0;
//}
////
// 
// 
//#include <stdio.h>
//#include <math.h>
//int main()
//{ double m, n;
//while(~scanf("%lf %lf", &n, &m)) 
//{
//    double sum = 0;
//    while (m-- > 0) {
//    sum += n;//从自身开始以及每次的平方根进行求和
//    n = sqrt(n);//n成为当前自身的平方根 
//    }
//    printf("%.2lf\n", sum); 
//}
//return 0; }

//
//
////寻找奇数
//#include<stdio.h>
//int main()
//{
//    int n, ai = 0;
//    int ans = 0;
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &ai);
//        ans ^= ai;
//    }
//    printf("%d\n", ans);
//    return 0;
//}


//寻找峰值
//应用二分查找法
int findPeakElement(int* nums, int numsLen)
{
    int left = 0;
    int right = numsLen - 1;
    while (left < right)
    {
        int mid = ((right - left) >> 1) + left; //防止直接相加发生溢出
        if (nums[mid] < nums[mid + 1])
            left = mid + 1;
        else
            right = mid;
    }
    return left;
}

