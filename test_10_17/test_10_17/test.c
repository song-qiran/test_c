#define _CRT_SECURE_NO_WARNINGS 1
////求奇数和
//#include<stdio.h>
//int main()
//{
//	int n, sum = 0;
//	scanf("%d", &n);
//	while (n > 0) {
//		if (n % 2 != 0)
//			sum += n;
//		scanf("%d", &n);
//	}
//	printf("%d", sum);
//	return 0;
//}




// 求给定精度的简单交错序列部分和
#include<stdio.h>
int main()
{
    int i = 1;
    double sum = 0;
    double eps = 0;
    scanf("%lf", &eps);
    for (i = 1; 1.0 / i > eps; i += 3)
    {
        if (i % 2 == 0)
            sum -= 1.0 / i;
        else
            sum += 1.0 / i;
    }
    printf("%.6lf", sum);
    return 0;
}



//求整数的位数及各位数字之和
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int i, sum = 0;
//    int count = 0;
//    scanf("%d", &n);
//    while (n)
//    {
//        i = n % 10;
//        sum += i;
//        count++;
//        n /= 10;
//    }
//    printf("%d %d", count, sum);
//    return 0;
//}


//链接辗转相除法
//m 和 n求最大公因数（假设m大于n），先用 m 除以 n ，
//如果余数 r 为 0 ，则 n 就是最大公因数，否则，将 n 给了 m ，将 r 给了 n ，
//再用 m 除以 n ，如果余数 r 为 0 ，则n为最大公因数，
//否则重复执行上述操作，直至 r 为 0 ，此时的 n 就是 m 和 n 的最大公因数。
