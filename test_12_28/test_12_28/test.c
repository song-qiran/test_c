#define _CRT_SECURE_NO_WARNINGS 1
//希尔排序算法的实现
void ShellSort(int* arr, int size)
{
    int gap = size;
    while (gap > 1)
    {
        gap = gap / 3 + 1;	//调整希尔增量
        int i = 0;
        for (i = 0; i < size - gap; i++)	//从0遍历到size-gap-1
        {
            int end = i;
            int temp = arr[end + gap];
            while (end >= 0)
            {
                if (arr[end] > temp)
                {
                    arr[end + gap] = arr[end];
                    end -= gap;
                }
                else
                {
                    break;
                }
            }
            arr[end + gap] = temp;	//以 end+gap 作为插入位置
        }
    }
}
#include<stdio.h>
#include<stdlib.h>
typedef  int  KeyType;
typedef  struct {
    KeyType* elem; /*elem[0]一般作哨兵或缓冲区*/
    int Length;
}SqList;
void  CreatSqList(SqList* L);/*待排序列建立，由裁判实现，细节不表*/
void  ShellInsert(SqList L, int dk);
void  ShellSort(SqList L);

int main()
{
    SqList L;
    int i;
    CreatSqList(&L);
    ShellSort(L);
    for (i = 1; i <= L.Length; i++)
    {
        printf("%d ", L.elem[i]);
    }
    return 0;
}
void   ShellSort(SqList L)
{
    /*按增量序列dlta[0…t-1]对顺序表L作Shell排序,假设规定增量序列为5,3,1*/
    int k;
    int dlta[3] = { 5,3,1 };
    int t = 3;
    for (k = 0; k < t; ++k)
        ShellInsert(L, dlta[k]);
}
/*你的代码将被嵌在这里 */
