#define _CRT_SECURE_NO_WARNINGS 1
//求 1 - n 之间偶数的和
//#include<iostream>
//using namespace std;
//int main()
//{
//    int n, i, sum = 0;
//    cin >> n;
//    for (i = 2; i <= n; i += 2)
//        sum += i;
//    cout << sum;
//
//    return 0;
//}

//计算一个数的阶乘
//#include<iostream>
//using namespace std;
//int main()
//{
//    int n, i;
//    long int mul = 1;
//    cin >> n;
//    for (i = 1; i <= n; i++)
//    {
//        mul *= i;
//    }
//    cout << mul;
//    return 0;
//}

//输出水仙花数
//#include<iostream>
//using namespace std;
//int main()
//{
//    int i = 0;
//    int k, n, m;
//    for (i = 100; i <= 999; i++)
//    {
//        k = i % 10;
//        m = i / 10 % 10;
//        n = i / 100;
//        if (k * k * k + m * m * m + n * n * n == i)
//            cout << i << endl;
//    }
//    return 0;
//}

//打印乘法表
//#include<iostream>
//using namespace std;
//int main()
//{
//    int n;
//    cin >> n;
//    int i, j;
//    for (i = 1; i <= n; i++)
//    {
//        for (j = 1; j <= i; j++)
//        {
//            cout << j << " " << "*" << " " << i << " " << "=" << " " << i * j;
//            cout << "    ";
//        }
//        cout << endl;
//    }
//    return 0;
//}

//规律数列求和
//#include<iostream>
//using namespace std;
//int main()
//{
//    long int sum = 0;
//    long int k = 0;
//    int i;
//    for (i = 0; i < 10; i++)
//    {
//        k = k * 10 + 9;
//        sum += k;
//
//    }
//    cout << sum;
//    return  0;
//}

// 计算小球走过的路程和反弹高度
//#include <iostream>
//#include <iomanip>
//using namespace std;
//
//int main() {
//
//    // 下落的高度和落地的次数
//    double h;
//    int n;
//
//    cin >> h;
//    cin >> n;
//
//    // write your code here......
//    double sum = 0;
//    for (int i = 0; i < n; i++) {
//        sum += h;
//        if (i > 0) sum += h;
//        h /= 2.0;
//    }
//    cout << fixed << setprecision(1) << sum << " " << setprecision(1) << h << endl;   //保留一位小数
//    return 0;
//}
