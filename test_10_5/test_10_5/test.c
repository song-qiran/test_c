#define _CRT_SECURE_NO_WARNINGS 1
//使用函数输出水仙花数
#include <stdio.h>

int narcissistic(int number);
void PrintN(int m, int n);

int main()
{
    int m, n;

    scanf("%d %d", &m, &n);
    if (narcissistic(m)) printf("%d is a narcissistic number\n", m);
    PrintN(m, n);
    if (narcissistic(n)) printf("%d is a narcissistic number\n", n);

    return 0;
}

/* 你的代码将被嵌在这里 */
int narcissistic(int number) {
	int exm, sum = 0, cnt = 0;
	exm = number;
	while (exm) {
		cnt++;
		exm /= 10;
	}
	exm = number;
	int i;
	while (exm) {
		i = exm % 10;
		sum += pow(i, cnt);
		exm /= 10;
	}
	if (sum == number) {
		return 1;
	}
	else {
		return 0;
	}
}

void PrintN(int m, int n) {
	for (int i = m + 1; i < n; i++) {
		if (narcissistic(i)) {
			printf("%d\n", i);
		}
	}
}
