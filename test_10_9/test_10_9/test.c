#define _CRT_SECURE_NO_WARNINGS 1
//使用函数实现字符串部分复制
//void strmcpy(char* t, int m, char* s)
//{
//    t = t + m - 1;
//    while (*t != '\0')
//    {
//        *s = *t;
//        s++;
//        t++;
//    }
//    *s = '\0';
//}
//#include <stdio.h>
//#define MAXN 20
//
//void strmcpy(char* t, int m, char* s);
//void ReadString(char s[]); /* 由裁判实现，略去不表 */
//
//int main()
//{
//    char t[MAXN], s[MAXN];
//    int m;
//
//    scanf("%d\n", &m);
//    ReadString(t);
//    strmcpy(t, m, s);
//    printf("%s\n", s);
//
//    return 0;
//}

/* 你的代码将被嵌在这里 */



////删除字符
//void delchar(char* str, char c)
//{
//    int flag;
//    int n = strlen(str);
//    int i, j;
//    for (i = 0, j = 0; i < n; i++)
//    {
//        if (str[i] == c)
//            continue;
//        else
//        {
//            str[j] = str[i];
//            j++;
//        }
//
//    }
//    str[j] = '\0';
//
//}
//#include <stdio.h>
//#define MAXN 20
//
//void delchar(char* str, char c);
//void ReadString(char s[]); /* 由裁判实现，略去不表 */
//
//int main()
//{
//    char str[MAXN], c;
//
//    scanf("%c\n", &c);
//    ReadString(str);
//    delchar(str, c);
//    printf("%s\n", str);
//
//    return 0;
//}
//
/* 你的代码将被嵌在这里 */
//


//字符串的连接
#include <stdio.h>
#include <string.h>

#define MAXS 10

char* str_cat(char* s, char* t);

int main()
{
    char* p;
    char str1[MAXS + MAXS] = { '\0' }, str2[MAXS] = { '\0' };

    scanf("%s%s", str1, str2);
    p = str_cat(str1, str2);
    printf("%s\n%s\n", p, str1);

    return 0;
}

/* 你的代码将被嵌在这里 */
char* str_cat(char* s, char* t)
{
    int len = strlen(s);
    int i = 0;
    for (i = 0; *(t + i) != '\0'; i++)
    {
        *(s + len + i) = *(t + i);
    }
    return s;
}
