#define _CRT_SECURE_NO_WARNINGS 1
//��������ǰ�����
class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {

        vector<int> res;
        if (!root) return res;

        stack<TreeNode*> s;
        s.push(root);

        while (!s.empty()) {
            TreeNode* node = s.top();
            s.pop();
            res.emplace_back(node->val);

            if (node->right) s.push(node->right);
            if (node->left) s.push(node->left);
        }

        return res;


    }
};
