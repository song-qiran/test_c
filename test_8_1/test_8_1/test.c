#define _CRT_SECURE_NO_WARNINGS 1
//小乐乐上课需要走n阶台阶，因为他腿比较长，所以每次可以选择走一阶或者走两阶，那么他一共有多少种走法
//类似青蛙爬楼梯
#include <stdio.h>

int walk(int n)
{
    if (n <= 2)
        return n;
    else
        return walk(n - 1) + walk(n - 2);
}

//《剑指offer》

int main()
{
    int n = 0;
    scanf("%d", &n);
    int ret = walk(n);
    printf("%d\n", ret);

    return 0;
}

