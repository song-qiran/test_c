import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 * Date: 2023-10-11
 * Time: 14:14
 */
public class Main {
    //8.存在连续三个奇数的数组
    //给你一个整数数组 arr，请你判断数组中是否存在连续三个元素都是奇数的情况：如果存在，请返回 true ；否则，返回 false 。
    //输入：arr = [2,6,4,1]
    //输出：false
    //输入：arr = [1,2,34,3,4,5,7,23,12]
    //输出：true
    public static void main(String[] args) {
       int[] arr8={1,2,34,3,4,5,7,23,12};
        System.out.println(change(arr8));
    }
    public  static  boolean change(int[] arr8){
        int cnt=0;
        for (int i = 0; i < arr8.length; i++) {
            if(arr8[i]%2!=0){
                cnt+=1;
            }else {
                cnt=0;
            }
        }
        if(cnt==2) {
            return true;
        }
        else{
           return  false;
            }
    }
    //7.多数元素
    //给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
    //你可以假设数组是非空的，并且给定的数组总是存在多数元素。
//示例 1：
//    输入：[3,2,3]
//    输出：3
    public static void main7(String[] args) {
        int[] arr7={3,2,3};
        int[]cp7=new int [5];
        for (int i = 0; i < arr7.length; i++) {
            cp7[arr7[i]]++;
        }
        int cntt = arr7.length/2;
        for (int i = 0; i < cp7.length; i++) {
            if(cp7[i]>cntt){
                System.out.println(i);
            }
        }
    }


    //6.只出现一次的数字
    //给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。
    // 找出那个只出现了一次的元素。
    //示例  输入: [2,2,1]
    //输出: 1
    public static void main6(String[] args) {
        int[] arr6 = {1,2,1};
        int [] cpy = new int[arr6.length];
        for (int i = 0; i < arr6.length; i++) {
            cpy[arr6[i]]++;
        }
        for (int i = 0; i < cpy.length; i++) {
            if(cpy[i] == 1){
                System.out.println(i);
            }
        }
    }



    //5.两数之和
    //给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出
    // 和为目标值 target 的那 两个 整数，并返回它们的数组下标。
    //你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
    //你可以按任意顺序返回答案。
    //输入：nums = [2,7,11,15], target = 9
    //输出：[0,1]
    //解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。

    public static void main5(String[] args) {
        int[] nums = {2,7,11,15};
        int target = 9;
        System.out.println(sums(nums,target));
    }
    public  static  String sums(int[] nums,int target){
        String ret = "[";
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1 ; j < nums.length; j++) {
                if(nums[i] + nums[j] == target){
                    ret+= i;
                    ret+=",";
                    ret+= j ;
                    ret+="]";
                }
            }
        }
        return  ret;
    }


    //4.冒泡排序
    //给定一个整型数组, 实现冒泡排序(升序排序)
    public static void main4(String[] args) {
        int[] arr4 = {2,5,4,3,7};
        bubble_sort(arr4);
        System.out.println(Arrays.toString(arr4));
    }
    public static  void bubble_sort(int[] arr4){
        for (int i = 0; i < arr4.length-1; i++) {
            for (int j = 0; j < arr4.length-1-i; j++) {
                if(arr4[j]>arr4[j+1]){
                    int tmp = arr4[j];
                    arr4[j] = arr4[j+1];
                    arr4[j+1]= tmp;
                }
            }
        }
    }


    //3.使用Array库的二分查找
    public static void main3(String[] args) {
        int[] arr3 = {1,2,3,4,5,6};
        System.out.println(Arrays.binarySearch(arr3, 5));
    }


    //2.二分查找----自己编写代码
   // 给定一个有序整型数组, 实现二分查找
    public static void main2(String[] args) {
        int[] arr2 = {1,2,3,4,5,6,7};
        System.out.println(binary_sort(arr2,5));
    }
    public  static  int binary_sort(int[] arr2,int key){
        int left = 0;
        int right = arr2.length-1;
        while(left<=right){
            int m = (left+right)/2;
            if(arr2[m]>key){
                right = m-1;
            } else if (arr2[m]<key) {
                left = m+1;
            }else{
                return m;
            }
        }
        return  -1;
    }




 //1.奇数位于偶数之前
    //调整数组顺序使得奇数位于偶数之前。调整之后，不关心大小顺序。
    //
    //如数组：[1,2,3,4,5,6]
    //
    //调整后可能是：[1, 5, 3, 4, 2, 6]
 public static void main1(String[] args) {
     int [] arr1 = {1,2,3,4,5,6};
     int i = 0;
     int j = arr1.length-1;
     while(i<j){
         if(arr1[i]%2==0){
             if(arr1[j]%2!=0){
                 int tmp = arr1[i];
                 arr1[i] = arr1[j];
                 arr1[j] = tmp;
             }
         }
         i++;
         j--;
     }
     System.out.println(Arrays.toString(arr1));
 }




}