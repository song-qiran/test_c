#define _CRT_SECURE_NO_WARNINGS 1
//给定长度为n的只有小写字母的字符串s，
// 进行m次操作，每次将[l,r]范围内所有c1字符改成c2
// 输出操作完的字符串
//字符串的操作
#include<stdio.h>
int main() {
    int n, m, l, r;
    char s[1000] = { 0 }, c1, c2;
    scanf("%d %d\n%s", &n, &m, &s);
    for (int i = 0; i < m; i++) {  //根据m值进行循环
        scanf("%d %d %c %c\n", &l, &r, &c1, &c2);
        for (int j = l; j <= r; j++) {  //根据每次输入的l,r值进行循环
            if (s[j - 1] == c1) s[j - 1] = c2;  //根据c1,c2的值来进行修改
        }
    }
    printf("%s", s);  //输出最终结果
    return 0;
}
