#define _CRT_SECURE_NO_WARNINGS 1
//插入排序
//折半插入-仅适用于顺序表(哨兵模式，0下标位置空着的，用于存放当前i所指的元素值，元素从下标1开始往后存放，默认从第二个元素即下标2开始往后遍历到下标n)
void Insert_Sort(int arr[], int n) {
    int i, j, low, high, mid;
    for (i = 2; i <= n; i++) {  //依次将arr[2]~arr[n]插入到前面的已排序序列
        int a = arr[i];
        arr[0] = arr[i];   //将arr[i]暂存到arr[0]中
        int b = arr[0];
        low = 1;
        high = i - 1;
        while (low <= high) {//折半查找默认递增有序，还未结束
            mid = (low + high) / 2;//取中间点
            int c = arr[mid];
            int d = arr[0];
            if (arr[mid] > arr[0]) {//应查左半子表
                high = mid - 1;
            }
            else {//大于等于都应查右半子表，为了其稳定性
                low = mid + 1;
            }
        }

        for (j = i - 1; j >= high + 1; --j) {   //应将下标high+1或low~i-1的元素往右移动一位，空出插入位置
            arr[j + 1] = arr[j];
        }

        arr[high + 1] = arr[0];//插入操作
    }
}


//
int main() {
    //    int arr[] = {1, 3, 2, 8, 6};
    int arr[5];
    arr[0] = NULL;
    arr[1] = 1;
    arr[2] = 3;
    arr[3] = 8;
    arr[4] = 5;
    Insert_Sort(arr, 4);
    for (int i = 1; i <= 4; ++i) {
        printf("%d\t", arr[i]);
    }
}