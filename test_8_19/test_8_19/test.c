#define _CRT_SECURE_NO_WARNINGS 1
//实现交换数组
#include<stdio.h>
#include<stdlib.h>
int main()
{
	int i, a[5], b[5], t;
	printf("Please input the first array:");
	for (i = 0; i < 5; i++)
		scanf("%d", &a[i]);
	printf("Please input the second array:");
	for (i = 0; i < 5; i++)
		scanf("%d", &b[i]);
	for (i = 0; i < 5; i++)
	{
		t = a[i];
		a[i] = b[i];
		b[i] = t;
	}
	printf("交换后的数组:\n");
	printf("Please input the first array:");
	for (i = 0; i < 5; i++)
		printf("%d ", a[i]);
	printf("\n");
	printf("Please input the second array:");
	for (i = 0; i < 5; i++)
		printf("%d ", b[i]);
	printf("\n");
	return 0;
}
//交换两个变量，不创建临时变量
#include<stdio.h>
int main()
{
	int a = 10;
	int b = 20;
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
	printf("a=%d  b=%d", a, b);
	return 0;

