#define _CRT_SECURE_NO_WARNINGS 1
//在二叉树中找到两个节点的最近公共祖先
public int firstLowestCommonAncestor(TreeNode<Integer> root, int o1, int o2) {
    if (null == root) {
        return -1;
    }
    if (root.val == o1 || root.val == o2) {
        return root.val;
    }
    int left = firstLowestCommonAncestor(root.left, o1, o2);
    int right = firstLowestCommonAncestor(root.right, o1, o2);
    if (left == -1) {
        return right;
    }
    if (right == -1) {
        return left;
    }
    return root.val;
}