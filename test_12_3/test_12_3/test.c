#define _CRT_SECURE_NO_WARNINGS 1
//递增的整数序列链表的插入
List Insert(List L, ElementType X)
{
	struct Node* head = L, * p;
	p = (struct Node*)malloc(sizeof(struct Node));
	p->Next = NULL;
	p->Data = X;
	while (L->Next != NULL && L->Next->Data < X)  //只能用L不能用head
	{
		L = L->Next;
	}
	p->Next = L->Next;
	L->Next = p;
	return head;
}
#include <stdio.h>
#include <stdlib.h>

typedef int ElementType;
typedef struct Node* PtrToNode;
struct Node {
	ElementType Data;
	PtrToNode   Next;
};
typedef PtrToNode List;

List Read(); /* 细节在此不表 */
void Print(List L); /* 细节在此不表 */

List Insert(List L, ElementType X);

int main()
{
	List L;
	ElementType X;
	L = Read();
	scanf("%d", &X);
	L = Insert(L, X);
	Print(L);
	return 0;
}

/* 你的代码将被嵌在这里 */
