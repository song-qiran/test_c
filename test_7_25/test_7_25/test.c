#define _CRT_SECURE_NO_WARNINGS 1
//注意：要求情况下尽量在case后面加上break
#include<stdio.h>
int main()
{
	int n = 1;
	int b = 0;
	switch (n)
	{
	case 1:
		b = 30;
	case 2:
		b = 20;
	default:
		b = 200;
	}
	printf("%d", b);
	return 0;
}
//输出结果为200
//编写一个函数实现n的k次方，使用递归实现。
#include<stdio.h>
#include<stdlib.h> 
int power(int n, int k)
{
	if (k <= 0)
		return 1;
	else
		return n * power(n, k - 1);
}
int main()
{
	int n = 0;
	int k = 0;
	scanf("%d%d", &n, &k);
	int ret = power(n, k);
	printf("%d\n", ret);
	system("pause");
	return 0;
}