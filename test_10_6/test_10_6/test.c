#define _CRT_SECURE_NO_WARNINGS 1
//找两个数中最大者
#include <stdio.h>

int max(int a, int b);

int main()
{
    int a, b;

    scanf("%d %d", &a, &b);
    printf("max = %d\n", max(a, b));

    return 0;
}

int max(int a, int b)
{
    if (a > b)
        return a;
    else
        return b;
}

