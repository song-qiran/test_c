#define _CRT_SECURE_NO_WARNINGS 1
//��ԭ������
#include<stdio.h>
#include<stdlib.h>
typedef struct tree {
    char data;
    struct tree* left, * right;
}tree;
int treeheight(tree* root) {
    if (root == NULL)
        return 0;
    else {
        int len1 = treeheight(root->left);
        int len2 = treeheight(root->right);
        return (len1 > len2) ? len1 + 1 : len2 + 1;
    }
}
tree* createtree(char* pre, char* in, int n) {
    tree* root;
    root = (tree*)malloc(sizeof(struct tree));
    if (n == 0)
        return NULL;
    int i;
    root->data = pre[0];
    for (i = 0; i < n; i++)
        if (in[i] == root->data)
            break;
    root->left = createtree(pre + 1, in, i);
    root->right = createtree(pre + i + 1, in + i + 1, n - i - 1);
    return root;
}
int main() {
    int n;
    scanf("%d", &n);
    char str1[n];
    char str2[n];
    scanf("%s%s", str1, str2);
    char* pre = str1;
    char* in = str2;
    tree* root = createtree(pre, in, n);
    printf("%d", treeheight(root));
    return 0;
}

