#define _CRT_SECURE_NO_WARNINGS 1
//编程一
//自除数 是指可以被它包含的每一位数除尽的数。
//例如， 128 是一个自除数，
//因为 128 % 1 == 0 ， 128 % 2 == 0 ， 128 % 8 == 0 。
//还有，自除数不允许包含 0 。给定上边界和下边界数字，输出一个列表，列表的元素是边界
//（含边界）内所有的自除数。
#include <stdio.h> 
int main()
{
	int num;
	while(scanf("%d", &num)!=EOF)
{
		if (num == 0) {
	//0的情况特殊处理，因为0不会进入while循环计算余数，因此不会被打印 
	printf("%d", num % 10);
	continue;
}
		while(num > 0)
{
	printf("%d", num % 10);//打印一个数字的个位数 129 % 10 得到9 num /= 10;
						   //通过除以10的方式去掉个位数 例如：129/10 得到12
		}printf("\n"); 
	}return 0;
}


//编程二
//、给你一个长度为 n 的整数数组 nums ，
//其中 n > 1 ，返回输出数组 output ，其中 output[i] 等于 nums 中除
//nums[i] 之外其余各元素的乘积。

#include <stdio.h>
#include <string.h>
int main()
{ char str[10001] = {0};//字符串最长10000
  int row = 0;
  while(gets(str) > 0) 
 { char *ptr = str;
   char *word[10000] = {NULL}; 
   while(*ptr != '\0') {
	   //如果是个字母字符，则是单词的起始字符
	   if (('z' >= *ptr && *ptr >= 'a') || ('Z' >= *ptr && *ptr >= 'A'))
{		word[row++] = ptr;//保存每个单词的起始地
		//把本次的单词字母字符走完，直到遇到非字母字符
	   while(*ptr != '\0' && (('z' >= *ptr && *ptr >= 'a') ||('Z' >= *ptr && *ptr >= 'A'))) 
	   {
		   ptr++;
	   }
	   continue;//不能继续向下，因为下边的ptr++会跳过当前的非字母字符
	   }
	   *ptr = '\0';//把非字母的数据全部替换为结尾标志
	   ptr++;
   }
   for (int i = row - 1; i >= 0; i--) 
   { printf("%s ", word[i]);//针对所有单词的起始地址逆序开始打印即可

}printf("\n"); } }