#define _CRT_SECURE_NO_WARNINGS 1
//链表内指定区间反转
//方法->头插法迭代
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 */

class Solution {
public:
    /**
     *
     * @param head ListNode类
     * @param m int整型
     * @param n int整型
     * @return ListNode类
     */
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        //创建一个新表头
        ListNode* newhead = new ListNode(-1);
        newhead->next = head;

        ListNode* pre = newhead;
        ListNode* cur = head;
        for (int i = 1; i < m; ++i) //找到m的位置
        {
            pre = cur;
            cur = cur->next;
        }

        for (int i = m; i < n; ++i) //从m反转到n
        {
            ListNode* next = cur->next;
            cur->next = next->next;
            next->next = pre->next;
            pre->next = next;
        }
        return newhead->next;
    }
};
