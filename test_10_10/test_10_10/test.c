#define _CRT_SECURE_NO_WARNINGS 1
//递归求Fabonacci数列
#include <stdio.h>
int f(int n);
int main()
{
    int n;
    scanf("%d", &n);
    printf("%d\n", f(n));
    return 0;
}
/* 你的代码将被嵌在这里 */
int f(int n)
{
    if (n == 1)
        return 1;
    else if (n == 0)
        return 0;
    else
        return f(n - 2) + f(n - 1);
}   
//非递归求Fabonacci数列

int fib(int n)
{
	int n1 = 1;
	int n2 = 1;
	int ret = 0;
	if (n <= 2)//n等于1或2时。
	{
		return 1;
	}
	for (int i = 3; i <= n; i++)//通过循环计算n>3时。
	{
		ret = n1 + n2;
		n1 = n2;
		n2 = ret;
	}
	return ret;
}
int main()
{
	int n = 0;
	int ret = 0;
	scanf("%d", &n);
	ret = fib(n);
	printf("%d\n", ret);
	system("pause");
	return 0;
}

