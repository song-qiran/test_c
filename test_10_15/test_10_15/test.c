#define _CRT_SECURE_NO_WARNINGS 1
////输出三角形面积和周长
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//    int a, b, c;
//    scanf("%d %d %d", &a, &b, &c);
//    if (a < b)
//    {
//        int temp;
//        temp = b;
//        b = a;
//        a = temp;
//    }
//    if ((a + b > c) && (a - b < c))
//    {
//        double area, s;
//        s = (a + b + c) / 2;
//        area = sqrt(s * (s - a) * (s - b) * (s - c));
//        double perimeter;;
//        perimeter = a + b + c;
//        printf("area = %.2lf; perimeter = %.2lf", area, perimeter);
//    }
//    else
//    {
//        printf("These sides do not correspond to a valid triangle");
//    }
//
//    return 0;
//}

////输出闰年
//#include<stdio.h>
//int is_leap(int x)
//{
//    if ((x % 4 == 0 && x % 100 != 0) || (x % 400 == 0))
//        return 1;
//    else
//        return 0;
//
//}
//int main()
//{
//    int year = 0;
//    scanf("%d", &year);
//    if (year <= 2000 || year > 2100)
//        printf("Invalid year!");
//    else
//    {
//        int i;
//        int flag = 0;
//        for (i = 2001; i <= year; i++)
//        {
//            int k = is_leap(i);
//            if (k)
//            {
//                printf("%d\n", i);
//                flag++;
//            }
//        }
//        if (flag == 0)
//        {
//            printf("None");
//        }
//    }
//
//    return 0;
//}
//高速公路超速处罚

#include<stdio.h>
int main()
{
    double speed, stad;
    scanf("%lf %lf", &speed, &stad);
    if (speed < 1.1 * stad)
    {
        printf("OK\n");
    }
    else if (speed < 1.5 * stad)
    {
        printf("Exceed %.0lf%%. Ticket 200\n", (speed - stad) / stad * 100);
    }
    else
        printf("Exceed %.0lf%%. License Revoked\n", (speed - stad) / stad * 100);
    return 0;
}