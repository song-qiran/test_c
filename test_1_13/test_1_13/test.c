#define _CRT_SECURE_NO_WARNINGS 1
//斐波那契数列
#include <stdio.h>
int fib(int n)
{
	int f1 = 1;
	int f2 = 1;
	int f3 = 0;
	if (n == 1 || n == 2)//处理特殊的第一项和第二项
	{
		return 1;
	}
	for (int i = 2; i < n; i++)
	{
		f3 = f1 + f2 >= 1000000007 ? (f1 + f2) % 1000000007 : f1 + f2;
		f1 = f2;
		f2 = f3;
	}
	return f3;
	/*第一遍做的时候把这个直接写成了return f3%1000000007,这样不对,当第n项值大于1000000007时,
	第n+1项的值就会和题目要求的不符合,
	所以在要将f3赋值给f2之前就要取模*/
}
int main()
{
	//测试用例
	printf("%d\n", fib(1));
	printf("%d\n", fib(2));
	printf("%d\n", fib(3));
	printf("%d\n", fib(4));
	printf("%d\n", fib(5));
	printf("%d\n", fib(6));
	printf("%d\n", fib(48));
	return 0;
}
