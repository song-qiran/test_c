#define _CRT_SECURE_NO_WARNINGS 1
//交换节点的单链表的排序
//1 、插入排序：
//=============插入排序====================
void insertSort(list mylist)
{
    if ((mylist->next == NULL) || (mylist->next->next == NULL))
    {
        return;
    }

    node* head, * p1, * prep1, * p2, * prep2, * temp;
    head = mylist;
    prep1 = head->next;
    p1 = prep1->next;
    //prep1和p1是否需要手动后移
    bool flag;

    while (p1 != NULL)
    {
        flag = true;
        temp = p1;
        //由于是单向链表，所以只能从头部开始检索
        for (prep2 = head, p2 = prep2->next; p2 != p1; prep2 = prep2->next, p2 = p2->next)
        {
            //发现第一个较大值
            if (p2->val > p1->val)
            {
                p1 = p1->next;
                prep1->next = p1;
                prep2->next = temp;
                temp->next = p2;
                flag = false;
                break;
            }
        }
        //手动后移prep1和p1
        if (flag)
        {
            prep1 = prep1->next;
            p1 = p1->next;
        }
    }
}
//=============插入排序====================
//2、冒泡排序：
//=============冒泡排序====================
void bubbleSort(list mylist)
{
    if ((mylist->next == NULL) || (mylist->next->next == NULL))
    {
        return;
    }

    node* head, * pre, * cur, * next, * end, * temp;
    head = mylist;
    end = NULL;
    //从链表头开始将较大值往后沉
    while (head->next != end)
    {
        for (pre = head, cur = pre->next, next = cur->next; next != end; pre = pre->next, cur = cur->next, next = next->next)
        {
            //相邻的节点比较
            if (cur->val > next->val)
            {
                cur->next = next->next;
                pre->next = next;
                next->next = cur;
                temp = next;
                next = cur;
                cur = temp;
            }
        }
        end = cur;
    }
}
//=============冒泡排序====================

//简单选择排序：
.//=============简单选择排序================
void selectSort(list mylist)
{
    if ((mylist->next == NULL) || (mylist->next->next == NULL))
    {
        return;
    }

    node* head, * prep1, * p1, * prep2, * p2, * premin, * min, * temp;
    head = mylist;
    for (prep1 = head, p1 = prep1->next; p1->next != NULL; prep1 = prep1->next, p1 = prep1->next)
    {
        //保存最小节点
        premin = prep1;
        min = p1;
        for (prep2 = p1, p2 = prep2->next; p2 != NULL; prep2 = prep2->next, p2 = prep2->next)
        {
            if (min->val > p2->val)
            {
                premin = prep2;
                min = p2;
            }
        }

        if (p1 != min)
        {
            //一定要注意这个情况
            if (p1->next == min)
            {
                temp = min->next;
                prep1->next = min;
                min->next = p1;
                p1->next = temp;
            }
            else {
                temp = min->next;
                prep1->next = min;
                min->next = p1->next;
                premin->next = p1;
                p1->next = temp;
            }
        }
    }
}
//=============简单选择排序================

