#define _CRT_SECURE_NO_WARNINGS 1
//使用函数求素数的和
#include <stdio.h>
#include <math.h>

int prime(int p)
{
    int i, n;
    if (p >= 2)
    {
        n = sqrt(p);
        for (i = 2; i <= n; i++)
        {
            if (p % i == 0)
            {
                break;
            }
        }
        if (i > n)
            return 1;
        else
            return 0;
    }
    else//如果p小于2则不是素数 
        return 0;
}

int PrimeSum(int m, int n)
{
    int sum = 0;
    for (int i = m; i <= n; i++)
    {
        if (prime(i) == 1)//如果是素数 
        {
            sum += i;
        }
    }
    return sum;
}

int main()
{
    int m, n, p;
    scanf("%d %d", &m, &n);
    printf("Sum of ( ");
    for (p = m; p <= n; p++)
    {
        if (prime(p) != 0)
        {
            printf("%d ", p);
        }
    }
    printf(") = %d\n", PrimeSum(m, n));
    return 0;
}
