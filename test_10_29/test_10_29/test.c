#define _CRT_SECURE_NO_WARNINGS 1
//函数实现字符串逆序

#include <stdio.h>
#define MAXS 20


void f(char* p)
{
    int k = 0, i = 0;
    while (p[i] != '\0')
    {
        k++;
        i++;
    }
    int a = 0, b = k - 1;
    while (a < b)
    {
        char tmp = p[b];
        p[b] = p[a];
        p[a] = tmp;
        a++;
        b--;
    }
}
void f(char* p);
//void ReadString(char* s); /* 由裁判实现，略去不表 */

int main()
{
    char s[MAXS];

    //ReadString(s);
    f(s);
    printf("%s\n", s);

    return 0;
}

