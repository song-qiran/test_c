#define _CRT_SECURE_NO_WARNINGS 1
//求m到n之和
//#include <stdio.h>
//
//int sum(int m, int n);
//
//int main()
//{
//    int m, n;
//
//    scanf("%d %d", &m, &n);
//    printf("sum = %d\n", sum(m, n));
//
//    return 0;
//}
//
//int sum(int m, int n)
//{
//    int sum = 0;
//    int i = 0;
//    for (i = m; i <= n; i++)
//    {
//        sum += i;
//    }
//    return sum;
//}
    //使用函数输出一个整数的逆序数
//#include <stdio.h>
//
//int reverse(int number);
//
//int main()
//{
//    int n;
//
//    scanf("%d", &n);
//    printf("%d\n", reverse(n));
//
//    return 0;
//}
//
//int reverse(int number)
//{
//    int flag = 1;
//    int x, sum = 0;
//    if (number < 0)
//    {
//        number = -number;
//        flag *= -1;
//    }
//    while (number > 0)
//    {
//        x = number % 10;
//        sum = sum * 10 + x;
//        number /= 10;
//    }
//    sum *= flag;
//    return sum;
//}
//使用函数求特殊a串数列和
//#include <stdio.h>
//
//int fn(int a, int n);
//int SumA(int a, int n);
//
//int main()
//{
//    int a, n;
//
//    scanf("%d %d", &a, &n);
//    printf("fn(%d, %d) = %d\n", a, n, fn(a, n));
//    printf("s = %d\n", SumA(a, n));
//
//    return 0;
//}
//
//int fn(int a, int n)
//{
//    int i, sum = 0;
//    for (i = 0; i < n; i++)
//    {
//        sum = sum * 10 + a;
//    }
//    return sum;
//}
//
//int SumA(int a, int n)
//{
//    int i, sum, sumA = 0;
//    for (i = 0; i < n; i++)
//    {
//        sum = sum * 10 + a;
//        sumA += sum;
//    }
//    return sumA;
//}
//在数组中查找指定元素


#include <stdio.h>
#define MAXN 10

int search(int list[], int n, int x);

int main()
{
    int i, index, n, x;
    int a[MAXN];

    scanf("%d", &n);
    for (i = 0; i < n; i++)
        scanf("%d", &a[i]);
    scanf("%d", &x);
    index = search(a, n, x);
    if (index != -1)
        printf("index = %d\n", index);
    else
        printf("Not found\n");

    return 0;
}

/* 你的代码将被嵌在这里 */
int search(int list[], int n, int x)
{
    if (n <= 0)
        return -1;
    else
    {
        int i = 0;
        for (i = 0; i < n; i++)
        {
            if (list[i] == x)
                return i;
        }
        if (list[i] != x)
            return -1;
    }
}


//判断回文字符串
#include <stdio.h>
#include <string.h>

#define MAXN 20
typedef enum { false, true } bool;

bool palindrome(char* s);

int main()
{
    char s[MAXN];

    scanf("%s", s);
    if (palindrome(s) == true)
        printf("Yes\n");
    else
        printf("No\n");
    printf("%s\n", s);

    return 0;
}

/* 你的代码将被嵌在这里 */
bool palindrome(char* s)
{
    int len = strlen(s);
    int i, flag = 1;
    int right = len - 1;
    for (i = 0; i < len; i++, right--)
    {
        if (s[i] != s[right])
            flag = 0;
    }
    if (flag)
        return true;
    else
        return false;
}
