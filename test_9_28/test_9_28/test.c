#define _CRT_SECURE_NO_WARNINGS 1
//水仙花数
#include <stdio.h>
int main() {
    int a, b;
    while ((scanf("%d %d", &a, &b)) != EOF) {
        int j = 0;
        for (int i = a; i <= b; i++) {
            int x = i / 100, y = (i % 100) / 10, z = i % 10;//获取个，十，百位
            if (x * x * x + y * y * y + z * z * z == i) {
                j++;
                printf("%d ", i);
            }
        }
        if (j == 0) printf("no\n");
        else printf("\n");
    }
}

