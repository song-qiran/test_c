#define _CRT_SECURE_NO_WARNINGS 1
//memcpy函数模拟实现
// int my_memcmp(const void* ptr1, const void* ptr2, size_t num)
{
assert(ptr1&& ptr2);
const char* ch_ptr1 = (char*)ptr1;
const char* ch_ptr2 = (char*)ptr2;
while (num && *ch_ptr1 && *ch_ptr2)
{
	if (*((unsigned char*)ch_ptr1) > *((unsigned char*)ch_ptr2)) //注意库中的都是转成无符号整数
	{
		return 1;
	}
	else if (*((unsigned char*)ch_ptr1) < *((unsigned char*)ch_ptr2))
	{
		return -1;
	}
	else
	{
		ch_ptr1++;
		ch_ptr2++;
		num--;
	}
}
if (num == 0)
return 0;
if (*ch_ptr1 == '\0')
return -1;
if (*ch_ptr2 == '\0')
return 1;
}

//memmove函数模拟实现
void* my_memmove(void* dst, const void* src, size_t num)
{
	assert(dst && src);
	char* ch_dst = dst;
	const char* ch_src = src;
	if (ch_src < ch_dst && ch_src + num > ch_dst) // 从后往前拷贝
	{
		for (int i = num - 1; i >= 0; i--)
		{
			ch_dst[i] = ch_src[i];
		}
	}
	else // 从前往后拷贝
	{
		for (int i = 0; i < num; i++)
		{
			ch_dst[i] = ch_src[i];
		}
	}
	return dst;
}
