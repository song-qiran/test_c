package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class Book {
    private String name;
    private String author;
    private int price;
    private String type;
    private boolean isBrrowed;

    public Book(String name, String author, int price, String type) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isBrrowed() {
        return isBrrowed;
    }

    public void setBrrowed(boolean brrowed) {
        isBrrowed = brrowed;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
               // ", isBrrowed=" + isBrrowed +
                ((isBrrowed==true)?"已借出":"未借出")+
                '}';
    }
}
