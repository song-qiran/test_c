package book;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class BookList {
    private Book[]  books = new  Book[10];
    private int  usedSize; //当前书架有几本书

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }

    public BookList(){
        books[0] = new Book("三国演义","罗贯中",19,"小说");
        books[1] = new Book("西游记","吴承恩",29,"小说");
        books[2] = new Book("水浒传","施耐庵",8,"小说");
        this.usedSize = 3;
    }
    public Book getBook(int pos){
        return books[pos];
    }

    public void setBook(int pos,Book book){
         books[pos] = book;
    }
}
