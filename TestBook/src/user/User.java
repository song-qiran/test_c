package user;

import book.BookList;
import operator.IOperation;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
 public  abstract class User {
    protected String name;

    public User(String name) {
        this.name = name;
    }
    public abstract int menu();
    public IOperation[] iOperations;//定义了接口数组

    public void doOperation(int choice, BookList bookList){
        this.iOperations[choice].work(bookList);
    }
}
