package operator;

import book.Book;
import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class ShowOperation implements  IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("打印所有图书！");
        int currentsize = bookList.getUsedSize();
        for (int i = 0; i < currentsize; i++) {
            Book book = bookList.getBook(i);
            System.out.println(book);
            }
        }
    }

