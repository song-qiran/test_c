package operator;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class ReturnOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("归还图书！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要归还的书的名字：--");
        String name = scanner.nextLine();//水浒传
        int currentsize = bookList.getUsedSize();
        for (int i = 0; i < currentsize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                System.out.println("找到这本书了");
                if(book.isBrrowed() == true) {
                    System.out.println("该书已经被借出");
                }else{
                book.setBrrowed(false);
                return;
            }

        }
        System.out.println("没有你要归还的书");
    }
    }
}
