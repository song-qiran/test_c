package operator;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class ExitOperation implements  IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("退出系统！");
        System.exit(0);
    }
}
