package operator;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class DelOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("删除图书！");
        System.out.println("请输入你要删除的书的名字：--");
        String name = scanner.nextLine();//水浒传
        int delIndex = -1;
        int i = 0;
        int currentsize = bookList.getUsedSize();
        for (; i < currentsize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                System.out.println("找到这本书了，开始进行删除操作！！！");
                delIndex = i;
                break;
            }
        }
        if(i==currentsize){
            System.out.println("没有你要查找的书");
        }
        for (int j = delIndex; j < currentsize-1 ; j++) {
            //[j] = [j+1]
            Book book = bookList.getBook(j+1);
            bookList.setBook(j,book);
        }
        bookList.setBook(currentsize-1,null);
        bookList.setUsedSize(currentsize-1);
        System.out.println("删除图书成功！！");
    }
}
