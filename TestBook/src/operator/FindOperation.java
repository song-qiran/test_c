package operator;

import book.Book;
import book.BookList;

import java.sql.SQLOutput;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class FindOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书！");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要查找的书的名字：--");
        String name = scanner.nextLine();//水浒传
        int currentsize = bookList.getUsedSize();
        for (int i = 0; i < currentsize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                System.out.println("找到这本书了");
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有你要查找的书");
    }
}
