package operator;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public interface IOperation {
    void work(BookList bookList);
}
