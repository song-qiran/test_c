#define _CRT_SECURE_NO_WARNINGS 1
//求二叉树的高度
typedef struct TNode* Position;
typedef Position BinTree;
struct TNode {
    ElementType Data;
    BinTree Left;
    BinTree Right;
};
#include <stdio.h>
#include <stdlib.h>

typedef char ElementType;
typedef struct TNode* Position;
typedef Position BinTree;
struct TNode {
    ElementType Data;
    BinTree Left;
    BinTree Right;
};

BinTree CreatBinTree(); /* 实现细节忽略 */
int GetHeight(BinTree BT);

int main()
{
    BinTree BT = CreatBinTree();
    printf("%d\n", GetHeight(BT));
    return 0;
}
/* 你的代码将被嵌在这里 */
int GetHeight(BinTree BT)
{
    int HL, HR, MAXH;
    if (BT == NULL)  return 0;
    else
    {
        HL = GetHeight(BT->Left);
        HR = GetHeight(BT->Right);
        MAXH = HL > HR ? HL : HR;
        return MAXH + 1;
    }
}   