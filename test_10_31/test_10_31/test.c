#define _CRT_SECURE_NO_WARNINGS 1
//按等级统计学生成绩
//
//int set_grade(struct student* p, int n)
//{
//    int i, cnt = 0;
//    for (i = 0; i < n; i++)
//    {
//        if (p[i].score >= 85 && p[i].score <= 100)
//            p[i].grade = 'A';
//        else if (p[i].score >= 70 && p[i].score <= 84)
//            p[i].grade = 'B';
//        else if (p[i].score >= 60 && p[i].score <= 69)
//            p[i].grade = 'C';
//        else if (p[i].score >= 0 && p[i].score <= 59)
//        {
//            p[i].grade = 'D';
//            cnt++;
//        }
//    }
//    return cnt;
//}
//#include <stdio.h>
//#define MAXN 10
//
//struct student {
//    int num;
//    char name[20];
//    int score;
//    char grade;
//};
//
//int set_grade(struct student* p, int n);
//
//int main()
//{
//    struct student stu[MAXN], * ptr;
//    int n, i, count;
//
//    ptr = stu;
//    scanf("%d\n", &n);
//    for (i = 0; i < n; i++) {
//        scanf("%d%s%d", &stu[i].num, stu[i].name, &stu[i].score);
//    }
//    count = set_grade(ptr, n);
//    printf("The count for failed (<60): %d\n", count);
//    printf("The grades:\n");
//    for (i = 0; i < n; i++)
//        printf("%d %s %c\n", stu[i].num, stu[i].name, stu[i].grade);
//    return 0;
//}
//
///* 你的代码将被嵌在这里 */


//使用递归函数计算1到n之和
//int sum(int n)
//{
//    int sum1 = 0;
//    if (n == 1)
//        return 1;
//    else if (n <= 0)   return 0;
//    else
//    {
//        sum1 = sum(n - 1) + n;
//    }
//    return sum1;
//}
//
//#include <stdio.h>
//
//int sum(int n);
//
//int main()
//{
//    int n;
//
//    scanf("%d", &n);
//    printf("%d\n", sum(n));
//
//    return 0;
//}


//判断满足条件的三位数
//#include <stdio.h>
//#include <math.h>
//int search(int n)
//{
//    int i = 101, j, m,cnt= 0;
//    for (i = 101; i <= n; i++)
//    {
//        m = i;
//        int k = (int)sqrt(i);
//        if (m ==k*k)
//        {
//            int a[3] = { 0 };
//            for (j = 0; j < 3; j++)
//            {
//                a[j] = m % 10;
//                m /= 10;
//            }
//            if (a[0] == a[1] || a[0] == a[2] || a[1] == a[2])
//                cnt++;
//        }
//    }
//    return cnt;
//}
//
//int main()
//{
//    int number;
//
//    scanf("%d", &number);
//    printf("count=%d\n", search(number));
//
//    return 0;
//}
//
//
//

//查找星期
//int getindex(char* s)
//{
//	int week;
//	char day[7][10] = { "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday" };
//	for (week = 0; week <= 6; week++)
//	{
//		if (strcmp(s, day[week]) == 0) break;
//	}
//	if (week == 7) week = -1;
//	return week;
//}
//
//
//#include <stdio.h>
//#include <string.h>
//
//#define MAXS 80
//
//int getindex(char* s);
//
//int main()
//{
//	int n;
//	char s[MAXS];
//
//	scanf("%s", s);
//	n = getindex(s);
//	if (n == -1) printf("wrong input!\n");
//	else printf("%d\n", n);
//
//	return 0;
//}
//





//输出月份英文名
//char *getmonth( int n )
//{
//char* str[12] = { "January","February","March",
//              "April","May","June","July","August",
//              "September","October","November","December" };
//if (n <= 0 || n > 12)  return 0;
//else
//{
//    return str[n + 1];
//}
//
//}
//#include <stdio.h>
//
//char* getmonth(int n);
//
//int main()
//{
//    int n;
//    char* s;
//
//    scanf("%d", &n);
//    s = getmonth(n);
//    if (s == NULL) printf("wrong input!\n");
//    else printf("%s\n", s);
//
//    return 0;
//}
