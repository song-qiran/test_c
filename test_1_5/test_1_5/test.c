#define _CRT_SECURE_NO_WARNINGS 1
//选择排序
		/*初始化左端、右端元素索引*/
int left = 0;
int right = len - 1;
while (left < right) {
	/*初始化最小值、最大值元素的索引*/
	int min = left;
	int max = right;
	for (int i = left; i <= right; i++) {
		/*标记每趟比较中最大值和最小值的元素对应的索引min、max*/
		if (arr[i] < arr[min])
			min = i;
		if (arr[i] > arr[max])
			max = i;
	}
	/*最大值放在最右端*/
	int temp = arr[max];
	arr[max] = arr[right];
	arr[right] = temp;
	/*此处是先排最大值的位置，所以得考虑最小值（arr[min]）在最大位置（right）的情况*/
	if (min == right)
		min = max;
	/*最小值放在最左端*/
	temp = arr[min];
	arr[min] = arr[left];
	arr[left] = temp;
	/*每趟遍历，元素总个数减少2，左右端各减少1，left和right索引分别向内移动1*/
	left++;
	right--;
}
