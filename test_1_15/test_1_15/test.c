#define _CRT_SECURE_NO_WARNINGS 1
//二叉树的镜像
//法一
public class Solution {
    private TreeNode temp;
    public void Mirror(TreeNode root) {
        if (root == null) {
            return;
        }
        temp = root.left;
        root.left = root.right;
        root.right = temp;
        Mirror(root.left);
        Mirror(root.right);
    }
}
//法二
public class Solution {

    public void Mirror(TreeNode root) {
        // 为空则结束
        if (root == null) {
            return;
        }
        // 假设 root 的左右子树都已经完成镜像了
        // 那就让左右子树交换
        swap(root);
        // 左子树做镜像操作
        Mirror(root.left);
        // 右子树做镜像操作
        Mirror(root.right);
    }

    private void swap(TreeNode node) {
        TreeNode temp = null;
        temp = node.left;
        node.left = node.right;
        node.right = temp;
    }
}
