import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {


    //2222给出一个有序的整数数组 A 和有序的整数数组 B ，请将数组 B 合并到数组 A 中，变成一个有序的升序数组
    public class Solution {
        public void merge(int A[], int m, int B[], int n) {
            int i = m - 1;
            int j = n - 1;
            int k = m + n - 1;
            while (i >= 0 && j >= 0) {
                if (A[i] < B[j]) {
                    A[k] = B[j];
                    k--;
                    j--;
                }
                else{
                    A[k] = A[i];
                    k--;
                    i--;
                }
            }
            while(i>=0){
                A[k] = A[i];
                i--;
                k--;
            }
            while(j>=0){
                A[k] = B[j];
                j--;
                k--;

            }

        }
    }



    //1111去重字符串，并按照顺序打印输出
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String s = in.nextLine();
            String ret = func(s);
            System.out.println(ret);
        }
    }
    public static String func(String s1){
        int [] arr = new int[155];
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<s1.length();i++){
            char ch = s1.charAt(i);
            if(arr[ch] == 0){
                sb.append(ch);
                arr[ch] = 1;
            }
        }
        return sb.toString();
    }

}