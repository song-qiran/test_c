import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */

public class test {




    //leetcode
    // 数组nums包含从0到n的所有整数，但其中缺了一个。请编写代码找出那个缺失的整数。你有办法在O(n)时间内完成吗？
    public static void main1(String[] args) {
        int[] arr = {3,0,1};
        //int ret = missingNumber(arr);
    }

    class Solution {
        public int missingNumber(int[] nums) {
            int n = nums.length;
            int total = n * (n + 1) / 2;
            int arrSum = 0;
            for (int i = 0; i < n; i++) {
                arrSum += nums[i];
            }
            return total - arrSum;
        }
    }

}
