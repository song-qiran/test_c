/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
//泛型的静态方法
class Alg2{
    public static <T extends  Comparable<T>> T findmax(T[] arr){
        T max = arr[0];
        for (int i = 1; i <arr.length; i++) {
            if(max.compareTo(arr[i])<0){
                max = arr[i];
            }
        }
        return max;
    }

}


//写一个泛型类，其中有一个方法，求数组中的最大值
class Alg<T extends  Comparable<T>>{
    public T findmax(T[] arr){
        T max = arr[0];
        for (int i = 1; i <arr.length; i++) {
            if(max.compareTo(arr[i])<0){
                max = arr[i];
            }
        }
    return max;
    }
}
public class test2 {

    public static void main(String[] args) {
        //Alg<Integer> alg2 = new Alg<>();
        Integer[] Arr ={1,2,3,4,5};
        Integer max =Alg2.findmax(Arr);
        System.out.println(max);
    }
    public static void main1(String[] args) {
        Alg<Integer> alg = new Alg<>();
        Integer[] Arr ={1,2,3,4,5};
        Integer max =alg.findmax(Arr);
        System.out.println(max);
    }
}
