#define _CRT_SECURE_NO_WARNINGS 1
//链表中倒数最后k个结点
	//快慢指针
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 *	ListNode(int x) : val(x), next(nullptr) {}
 * };
 */
class Solution {
public:
    ListNode* FindKthToTail(ListNode* pHead, int k) {
        // write code here
        //采用双指针法
        if (pHead == NULL && k <= 0)
        {
            return nullptr;
        }
        //定义fast和slow都指向第一个节点
        ListNode* fast = pHead;
        ListNode* slow = pHead;

        while (k--)
        {
            //首先让fast遍历k个节点，这样fast和slow就差k个节点
            if (fast == nullptr)
            {
                return nullptr;
            }
            fast = fast->next;
        }

        //当fast节点为nullptr时，slow为倒数第k个节点
        while (fast != nullptr)
        {
            fast = fast->next;
            slow = slow->next;
        }
        return slow;
    }
};
.

//数组法
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 *	ListNode(int x) : val(x), next(nullptr) {}
 * };
 */
    class Solution {
    public:
        ListNode* FindKthToTail(ListNode* pHead, int k) {
            // write code here
            vector<ListNode* > res;
            while (pHead)
            {
                res.push_back(pHead);
                pHead = pHead->next;
            }
            if (res.size() < k)
            {
                return nullptr;
            }
            return res[res.size() - k];
        }
};