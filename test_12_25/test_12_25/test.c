#define _CRT_SECURE_NO_WARNINGS 1
//另类循环队列
typedef int Position;
typedef struct QNode* PtrToQNode;
struct QNode {
    ElementType* Data;  /* 存储元素的数组   */
    Position Front;     /* 队列的头指针     */
    int Count;          /* 队列中元素个数   */
    int MaxSize;        /* 队列最大容量     */
};
typedef PtrToQNode Queue;
bool AddQ(Queue Q, ElementType X) {
    if (Q->Count == Q->MaxSize) {
        printf("Queue Full\n");
        return false;
    }
    Q->Count++;
    Q->Data[(Q->Front + Q->Count) % Q->MaxSize] = X;
    return true;
}
ElementType DeleteQ(Queue Q) {
    if (Q->Count == 0) {
        printf("Queue Empty\n");
        return ERROR;
    }
    Q->Count--;
    Q->Front = (Q->Front + 1) % Q->MaxSize;
    return Q->Data[Q->Front];
}