#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{	char c = 128;
//	printf("%u\n", c);
//	return 0;
//}
//int main()
//{
//	unsigned char a = 200;
//	unsigned char b = 100;
//	unsigned char c = 0;
//	c = a + b;
//	printf(" % d % d", a + b, c);
//	return 0;
//}
//int main()
//{
//	unsigned int a = 0x1234;
//	unsigned char b = *(unsigned char*)&a;
//	printf("%d", b);
//	return 0;
//}
//
//int main()
//{
//    char a[1000] = { 0 };
//    int i = 0;
//    for (i = 0; i < 1000; i++)
//    {
//        a[i] = -1 - i;
//    }
//    printf("%d", strlen(a));
//    return 0;
//}

//
//#include <stdio.h>
//#include <stdlib.h>
//#include <malloc.h>
//#include <windows.h>
//
//int main()
//{
//	int a, b, c, d, e;
//	int num = 0;
//	for (a = 1; a <= 5; a++)
//	{
//		for (b = 1; b <= 5; b++)
//		{
//			for (c = 1; c <= 5; c++)
//			{
//				for (d = 1; d <= 5; d++)
//				{
//					for (e = 1; e <= 5; e++)
//					{
//						if (((b == 1) + (a == 3) == 1)
//							&& ((b == 2) + (e == 4) == 1)
//							&& ((c == 1) + (d == 2) == 1)
//							&& ((c == 5) + (d == 3) == 1)
//							&& ((e == 4) + (a == 1) == 1))
//						{
//							num = 0;
//							num |= (1 << (a - 1));//a=3
//							num |= (1 << (b - 1));
//							num |= (1 << (c - 1));
//							num |= (1 << (d - 1));
//							num |= (1 << (e - 1));
//							while (num)  //有可能有并列，但是绝不可能有第一名第三名，没有第二名
//							{
//								if (num % 2 == 0)
//									break;
//								num = num / 2;
//							}
//							if (num == 0)
//							{
//								printf("a=%d b=%d c=%d d=%d e=%d\n", a, b, c, d, e);
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//	system("pause");
//	return 0;
//}

//杨辉三角
#include <stdio.h>
//杨辉三角
int main()
{
	int n;
	printf("请输入要打印的行数：");
	scanf("%d", &n);
	int a[100][100];
	for (int i = 0; i < n; i++)
	{
		a[i][0] = 1;
		a[i][i] = 1;    //先将杨辉三角每行的第一个和最后一个赋值为1 
	}

	for (int i = 2; i < n; i++)      //已知每个数等于它上方两数的之和 
	{
		for (int j = 1; j < i; j++)
		{
			a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
		}
	}


	for (int i = 0; i < n; i++)     //打印输出 
	{
		for (int j = 0; j <= i; j++)
		{
			printf("%d ", a[i][j]);
		}
		printf("\n");
	}
}
