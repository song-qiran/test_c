/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */

public class Main {



    //字符串查找
    public static void main(String[] args) {
        String s8 = "abcdefg";
        char c = s8.charAt(1);
        System.out.println(c);
    }
    //比较字符串
    public static void main2(String[] args) {
        String s2 = "abdc";
        String s3 = "svduv";
        //直接使用等号比较
        System.out.println(s2 == s3);  //false
        System.out.println(s2.equals(s3)); //false
        System.out.println("---------------");
        String s4 = new String("sbc");
        System.out.println(s2.equals(s3)); //false
        String s5 = new String("sbc");
        System.out.println(s4.equals(s5)); //true

        System.out.println("================");
        String s6 = "abc";
        String s7 = "abc";
        System.out.println(s6.equals(s7));   //true  引入常量池

    }
    //字符串三种构造方式
    public static void main1(String[] args) {
        //System.out.println("Hello world!");
        //方式1   直接定义
        String s1 = "hello ";
        System.out.println(s1);
        //方式2 使用new进行构造
        String s2 = new String("hellowa");
        System.out.println(s2);
        //方式3 使用字符串构造
        char [] chh =new char[]{'a','b','b'};
        String s3 = new String(chh);
        System.out.println(s3);
    }
}