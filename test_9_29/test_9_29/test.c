#define _CRT_SECURE_NO_WARNINGS 1

// 求简单交错幂级数的部分和


//递归实现
//double fn(double x, int n) {
//    int flag = 1;
//    if (n == 1) {
//        return x;
//    }
//    else {
//        return x - x * fn(x, n - 1);
//    }
//}


//实现x的n次方的函数
#include <stdio.h>

double mypow(double x, int n);

int main()
{
    double x;
    int n;

    scanf("%lf %d", &x, &n);
    printf("%f\n", mypow(x, n));

    return 0;
}
double mypow(double x, int n)
{
    int i;
    double b;
    b = 1;
    if (n == 0) { return 1; }
    else {
        for (i = 1; i <= n; i++) {
            b = b * x;
        }
        return b;
    }



}

