#define _CRT_SECURE_NO_WARNINGS 1
//求多项式在给定点的值
#include<stdio.h>
#include<math.h>
int main()
{
    int n, i;
    scanf("%d", &n);
    double a[n + 1];
    for (i = 0; i <= n; i++)
        scanf("%lf", &a[i]);
    double x;
    scanf("%lf", &x);
    double p = 0;
    for (i = 0; i <= n; i++)
        p = p + a[i] * pow(x, i);
    printf("%.4lf", p);
    return 0;
}