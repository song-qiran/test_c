#define _CRT_SECURE_NO_WARNINGS 1
//2的n次方运算
#include<stdio.h>
int main()
{
    int n = 0;
    scanf("%d", &n);
    printf("%d", 2 << n - 1);
    return 0;
}


//显示年龄
//问题：一年约有 3.156×107 s，要求输入您的年龄，显示该年龄合多少秒。
//数据范围： 0 < age \le 200 \0 < age≤200


//法一
//使用的是C语言
#include<stdio.h>
int main() {
    long int age, second;//需用到长整型
    scanf("%ld", &age);
    second = age * 31560000;
    printf("%ld", second);
    return 0;
}


//法二

int main()
{
    int age;
    //一年有3.156×10的7次方秒 =3156×10的4次方秒
    int time = 3156;//直接令时间为3156，就算人活500年也不会溢出

    scanf("%d", &age);
    time = age * time;
    printf("%d0000", time);//计算的结果再补最后的四个0
    return 0;
}
