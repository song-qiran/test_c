#define _CRT_SECURE_NO_WARNINGS 1
#include<bits/stdc++.h>
using namespace std;
typedef struct node
{
	int data;
	struct node* next;
}tree;
int main()
{
	ios::sync_with_stdio(false);
	struct node* head1, * head2, * p, * q, * s, * tail1, * tail2, * head3, * tail;
	head1 = new tree;
	head2 = new tree;
	head1->next = NULL;
	head2->next = NULL;
	tail1 = head1;
	tail2 = head2;
	int m, n, i;
	cin >> m >> n;
	for (i = 0; i <= m - 1; i++)
	{
		p = new tree;
		cin >> p->data;
		p->next = NULL;
		tail1->next = p;
		tail1 = p;
	}
	for (i = 0; i <= n - 1; i++)
	{
		q = new tree;
		cin >> q->data;
		q->next = NULL;
		tail2->next = q;
		tail2 = q;
	}
	head3 = new tree;
	head3->next = NULL;
	tail = head3;
	p = head1->next;
	q = head2->next;
	int c1 = 0, c2 = 0;
	while (p && q)
	{
		if (p->data < q->data)
		{
			s = new tree;
			s->data = p->data;
			p = p->next;
			tail->next = s;
			tail = s;
			c1++;
		}
		else
		{
			s = new tree;
			s->data = q->data;
			q = q->next;
			tail->next = s;
			tail = s;
			c2++;
		}
	}
	for (i = c1; i <= m - 1; i++)
	{
		s = new tree;
		s->data = p->data;
		p = p->next;
		tail->next = s;
		tail = s;
	}
	for (i = c2; i <= n - 1; i++)
	{
		s = new tree;
		s->data = q->data;
		q = q->next;
		tail->next = s;
		tail = s;
	}
	p = head3->next;
	while (p)
	{
		if (p->next == NULL)
		{
			cout << p->data << endl;
		}
		else
		{
			cout << p->data << " ";
		}
		p = p->next;
	}
	return 0;
}	