#define _CRT_SECURE_NO_WARNINGS 1
//线性探测法的查找函数
Position Find(HashTable H, ElementType Key) {
    int a = Key % H->TableSize;
    int count = 0;
    while (H->Cells[a].Info != Empty && count != H->TableSize) {
        count++;
        if (H->Cells[a].Data == Key)
            return a;
        else
            a = (a + 1) % H->TableSize;
    }
    if (H->Cells[a].Info == Empty)
        return a;
    else
        return ERROR;
}
#define MAXTABLESIZE 100000  /* 允许开辟的最大散列表长度 */
typedef int ElementType;     /* 关键词类型用整型 */
typedef int Index;           /* 散列地址类型 */
typedef Index Position;      /* 数据所在位置与散列地址是同一类型 */
/* 散列单元状态类型，分别对应：有合法元素、空单元、有已删除元素 */
typedef enum { Legitimate, Empty, Deleted } EntryType;

typedef struct HashEntry Cell; /* 散列表单元类型 */
struct HashEntry {
    ElementType Data; /* 存放元素 */
    EntryType Info;   /* 单元状态 */
};

typedef struct TblNode* HashTable; /* 散列表类型 */
struct TblNode {   /* 散列表结点定义 */
    int TableSize; /* 表的最大长度 */
    Cell* Cells;   /* 存放散列单元数据的数组 */
};
