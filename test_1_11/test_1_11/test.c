#define _CRT_SECURE_NO_WARNINGS 1
// 链表内指定区间反转
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 */
//法一
class Solution {
public:
    /**
     *
     * @param head ListNode类
     * @param m int整型
     * @param n int整型
     * @return ListNode类
     */
    ListNode* reverse(ListNode* head) {
        ListNode* pre = nullptr, * ne = head->next;
        while (head != nullptr) {
            ne = head->next;
            head->next = pre;
            pre = head;
            head = ne;
        }
        return pre;
    }
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        // write code here
        //添加虚拟头节点
        ListNode* fakeNode = new ListNode(-1);
        fakeNode->next = head;

        //找逆转序列的头节点的前一个节点pre，需走m-1步
        ListNode* pre = fakeNode;
        for (int i = 0; i < m - 1; i++) pre = pre->next;

        //找逆转序列的尾节点right，需再走m-n+1步
        ListNode* right = pre;
        for (int i = 0; i < n - m + 1; i++) right = right->next;

        //截取子链表
        ListNode* left = pre->next;
        ListNode* ne = right->next;

        //切断连接
        pre->next = nullptr;
        right->next = nullptr;

        //局部反转
        right = reverse(left);

        //接上
        pre->next = right;
        left->next = ne;

        return fakeNode->next;
    }
};


//法二
    /**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 */

class Solution {
public:
    /**
     *
     * @param head ListNode类
     * @param m int整型
     * @param n int整型
     * @return ListNode类
     */
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        // write code here
        //添加虚拟头节点
        ListNode* fakeNode = new ListNode(-1);
        fakeNode->next = head;

        //找待翻转链表的前驱pre，走m-1步
        ListNode* pre = fakeNode;
        for (int i = 0; i < m - 1; i++)  pre = pre->next;

        //运行n-m步，遍历待翻转区间，利用头插法将其倒序
        ListNode* p = pre->next;
        ListNode* ne = nullptr;

        for (int i = 0; i < n - m; i++) {
            ne = p->next;//ne为待拆分，并提至链头的节点
            p->next = ne->next;//此步必须在前，断开前一节点与ne节点的连接，使之连上ne后面的链表
            ne->next = pre->next;//拆分出来的节点连上pre后面的链
            pre->next = ne;//再使pre连上以拆分出来的节点开头的新链
        }
        return fakeNode->next;

    }
};
