#define _CRT_SECURE_NO_WARNINGS 1
//两个有序链表序列的合并
List Merge(List L1, List L2)
{
	List L, p, q, r;
	L = (List)malloc(sizeof(List));//创建新的头结点
	p = L1->Next;
	q = L2->Next;
	r = L;
	while (p != NULL && q != NULL)
	{
		if (p->Data < q->Data)
		{
			r->Next = p;//将第一个链表的结点接到新链表上
			r = p;
			p = p->Next;
		}
		else
		{
			r->Next = q;//将第二个链表的结点接到新链表上
			r = q;
			q = q->Next;
		}
	}
	r->Next = p ? p : q;//将没有扫描完的链表结点直接接到新链表上
	L1->Next = NULL;
	L2->Next = NULL;
	return L;
}