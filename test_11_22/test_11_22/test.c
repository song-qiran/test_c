#define _CRT_SECURE_NO_WARNINGS 1
//修理牧场
#include <stdio.h>
#include<stdbool.h>
void sort(int a[], int n) {
    int i, j, temp;
    for (i = 0; i < n - 1; i++) {
        for (j = i + 1; j < n; j++) {
            if (a[i] > a[j]) {
                temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
}
int main() {
    int a[10001];
    int n, i, sum = 0, temp = 0;
    scanf("%d", &n);
    for (i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    if (n == 1)
        printf("%d\n", 0);
    else if (n == 2)
        printf("%d\n", a[0] + a[1]);
    else {
        while (true) {
            sort(a, n);
            if (a[n - 2] == 0)
                break;
            for (i = 0; i < n; i++) {
                if (a[i] != 0) {
                    temp = a[i] + a[i + 1];
                    a[i] = 0; a[i + 1] = temp;
                    sum += temp;
                    break;
                }
            }
        }
        printf("%d\n", sum);
    }
    return 0;
}


// 斯德哥尔摩火车上的题
#include<stdio.h>
#include<string.h>
int main() {
    int i, la, lb, j = 0;
    char aa[10001], aayi[10001], bb[10001], bbyi[10001];
    la = strlen(gets(aa));       //gets()获得键盘输入的字符串，以换行符结束；strlen()求字符串长度
    for (i = 1; i < la; i++) {
        if ((aa[i] - '0') % 2 == (aa[i - 1] - '0') % 2)            //减 ‘0’是为了把ASCII码转换成对应数字
            aayi[j++] = aa[i] > aa[i - 1] ? aa[i] : aa[i - 1];   //三目运算，取二者中大的那个
    }
    lb = strlen(gets(bb));
    j = 0;
    for (i = 1; i < lb; i++) {
        if ((bb[i] - '0') % 2 == (bb[i - 1] - '0') % 2)            //同上
            bbyi[j++] = bb[i] > bb[i - 1] ? bb[i] : bb[i - 1];   //同上     
    }
    if (strcmp(aayi, bbyi) == 0)        //strcmp()函数是比较两个字符串是否相等
        printf("%s\n", aayi);
    else
        printf("%s\n%s\n", aayi, bbyi);
}

