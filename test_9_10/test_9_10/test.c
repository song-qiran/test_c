#define _CRT_SECURE_NO_WARNINGS 1
////模拟实现strcmp函数
//#include<stdio.h>
//#include<stdlib.h>
//#include<assert.h>
//char Mystrcmp(const char* dest, const char* src)
//{
//	while ((*dest != '\0') && (*src != '\0'))
//	{
//		if (*dest < *src)
//		{
//			return -1;
//		}
//		else if (*dest > *src)
//		{
//			return 1;
//		}
//		else
//		{
//			++dest;
//			++src;
//		}
//	}
//	if (*dest < *src)
//	{
//		return -1;
//	}
//	else if (*dest > *src)
//	{
//		return 1;
//	}
//	return 0;
//}
//
//int main()
//{
//	char dest[] = "hello world";
//	char src[] = "hello";
//	int ret = Mystrcmp(dest, src);
//	printf("%d\n", ret);
//	system("pause");
//	return 0;
//}


////模拟实现strcat函数
//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//void Storage(char* arr1, char* arr2, int size)        //交换函数的内容
//{
//	int i = 0;                             //定义一个循环变量
//	for (i = 0; i < size; i++)
//	{
//		arr1[i] = arr2[i];
//	}
//
//}
//Link()
//{
//	char arr[] = "abcd";
//	char arr1[] = "efg";
//	char arr2[80] = { 0 };
//	int size1 = strlen(arr);
//	int size2 = strlen(arr1);
//	Storage(&arr2[0], arr, size1);               //交换函数，在arr2数组里的第一个位置将arr字符串进行复制
//	Storage(&arr2[4], arr1, size2);            // 第一个字符串占了4个空间，所以第二个字符串的复制就从第五个位置开始复制。
//	printf("%s\n", arr2);
//}
//
//int main()
//{
//	Link();
//	system("pause");
//	return 0;
//}


//模拟实现strstr函数
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

const char* my_strstr(const char* str1, const char* str2) { //我们只是进行查找，不进行修改，所以使用 const 进行修饰
    assert(str1 != NULL && str2 != NULL);//使用断言来判断地址是否合法
    const char* start = str1;
    const char* tmp = str2;
    const char* check = start;
    while (*start) {
        check = start;//标记每次开始查找的地址
        //这个循环是进行字符串1的子字符串和字符串2进行比较
        while (*check == *tmp && *check != '\0' && *tmp != '\0') {
            check++;
            tmp++;
        }
        if (*tmp == '\0') {
            return start;
        }
        start++;
        tmp = str2;
    }
    return NULL;
}

int main() {
    char str1[] = "abcccdef";
    char str2[] = "ccd";
    printf("%s\n", my_strstr(str1, str2));

    return 0;
}
