#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <cmath>
using namespace std;
int main() {
	int n, m;
	cin >> n >> m;
	string s;
	while (m--) {
		int num = 1;
		cin >> s;
		for (int i = 0; i < s.length(); i++) {
			if (s[i] == 'n') {
				num += pow(2, n - 1 - i);
			}
		}
		cout << num << endl;
	}
}
