#define _CRT_SECURE_NO_WARNINGS 1
//浮点数的个位数字
//给定一个浮点数，要求得到该浮点数的个位数。

//方法一  ->自己
#include<stdio.h>
int main()
{
    double n = 0;
    scanf("%lf", &n);
    printf("%d\n",(int) n % 10);
    return 0;
}

//有点麻烦是里面0~200范围，丢掉范围限制代码可自行简化
#include<stdio.h>
int main()
{
    float a = 0.0;
    int b, c;
    scanf("%f", &a);
    if (a < 10)
    {
        b = (int)a;
        printf("%d\n", b);
    }
    else if (a > 10 && a < 100)
    {
        b = (int)a;
        c = b % 10;
        printf("%d", c);
    }
    else
    {
        b = (int)a;
        c = ((b % 100) % 10);
        printf("%d", c);
    }
    return 0;
}
