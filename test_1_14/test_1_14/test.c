#define _CRT_SECURE_NO_WARNINGS 1
//按之字形顺序打印二叉树
/*
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
    TreeNode(int x) :
            val(x), left(NULL), right(NULL) {
    }
};
*/
class Solution {
public:
    vector<vector<int> > Print(TreeNode* pRoot) {
        vector<vector<int> > res;
        vector<int> temp;
        stack<TreeNode*> even;
        stack<TreeNode*> odd;
        if (pRoot == NULL) return res;
        odd.push(pRoot);
        while (!even.empty() || !odd.empty()) {
            while (!odd.empty()) {
                TreeNode* t = odd.top();
                odd.pop();
                temp.push_back(t->val);
                if (t->left != NULL) even.push(t->left);
                if (t->right != NULL) even.push(t->right);
            }
            if (!temp.empty()) res.push_back(temp);
            temp.clear();
            while (!even.empty()) {
                TreeNode* t = even.top();
                even.pop();
                temp.push_back(t->val);
                if (t->right != NULL) odd.push(t->right);
                if (t->left != NULL) odd.push(t->left);
            }
            if (!temp.empty()) res.push_back(temp);
            temp.clear();
        }
        return res;
    }
};
    