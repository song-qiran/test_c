#define _CRT_SECURE_NO_WARNINGS 1
//个位数统计
//给定一个 k 位整数 N,请编写程序统计每种不同的个位数字出现的次数。
//例如：给定 N=100311，则有 2 个 0，3 个 1，和 1 个 3。
#include<stdio.h>
#include<string.h>
int main()
{
	int i, k, j;
	char s[1000];
	int b[10];
	for (i = 0; i < 10; i++)
	{
		b[i] = 0;
	}
	scanf("%s", s);
	for (j = 0; j < 10; j++)
	{
		for (k = 0; k < strlen(s); k++)
		{
			if (s[k] - 48 == j) {
				b[j]++;
			}
		}
	}
	for (i = 0; i < 10; i++)
	{
		if (b[i] != 0)
		{
			printf("%d:%d\n", i, b[i]);
		}
	}
	return 0;
}
