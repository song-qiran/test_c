#define _CRT_SECURE_NO_WARNINGS 1
//strcpy函数模拟实现
//#include<stdio.h>
//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;//处理'\0'
//}
//
//int main()
//{
//	char arr1[20] = "xxxxxxxxxxxxx";
//	char arr2[] = "hello";
//	my_strcpy(arr1, arr2);
//	printf("%s", arr1);
//	return 0;
//}
//

////strlen函数模拟实现
//#include<stdio.h>
//#include<assert.h>
//int my_strlen(const char* str)
//{
//    assert(str != NULL);
//    int count = 0;
//    while (*str)
//    {
//        count++;
//        str++;
//    }
//    return count;
//}
//
//int main()
//{
//    char str[] = "abcdef";
//    int len = my_strlen(str);
//    printf("%d", len);
//    return 0;
//}