#define _CRT_SECURE_NO_WARNINGS 1
//实现字符串的逆序
、#include<stdio.h>
int my_strlen(char* str)
{
	int count = 0;
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return count;
}
void reverse_string(char* str)
{
	int left = 0;
	int right = my_strlen(str) - 1;
	while (left < right)
	{
		char tmp = str[left];
		str[left] = str[right];
		str[right] = tmp;
		left++;
		right--;
	}
}
int main()
{
	char arr[] = "abcdef";//首先定义数组为字符串abcdef
	reverse_string(arr);//调用函数，使得数组arr内的字符串逆序调换
	printf("%s\n", arr);//打印逆序转换之后的字符串arr
	return 0;
}

#include<stdio.h>
int my_strlen(char* str)
{
	int count = 0;
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return count;
}
void reverse_string(char* str)
{
	char tmp = *str;
	int len = my_strlen(str);
	*str = *(str + len - 1);
	*(str + len - 1) = '\0';
	if (my_strlen(str + 1) >= 2)
	{
		reverse_string(str + 1);
	}
	*(str + len - 1) = tmp;
}
int main()
{
	char arr[] = "abcdef";//首先定义数组为字符串abcdef
	reverse_string(arr);//调用函数，使得数组arr内的字符串逆序调换
	printf("%s\n", arr);//打印逆序转换之后的字符串arr
	return 0;
}