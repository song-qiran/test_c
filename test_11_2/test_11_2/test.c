#define _CRT_SECURE_NO_WARNINGS 1
//判断上三角矩阵
#include <stdio.h>

int main() {
    int T;
    scanf("%d", &T);
    int k, i, j, n;
    int flag[100] = { 0 };
    for (k = 0; k < T; k++) {
        scanf("%d", &n);
        int a[n][n];
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                scanf("%d", &a[i][j]);
                if (i > j && a[i][j]) {
                    flag[k] = 1;//为1表示不是上三角形
                }
            }
        }
    }
    for (k = 0; k < T; k++) {
        if (!flag[k]) {
            printf("YES\n");
        }
        else { printf("NO\n"); }
    }
    return 0;
}



//打印杨辉三角
#include<stdio.h>
int main() {
    int n, i, j, a[10][10] = { 0 };
    scanf("%d", &n);
    for (i = 0; i < n; i++) {
        for (j = n; j > i + 1; j--) printf(" ");
        for (j = 0; j <= i; j++) {
            if (j == 0) a[i][j] = 1;
            else if (j <= (i + 1) / 2) a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
            else a[i][j] = a[i][i - j];
            printf("%4d", a[i][j]);
        }
        printf("\n");
    }
    return 0;
}