#define _CRT_SECURE_NO_WARNINGS 1
//打印沙漏
//所谓“沙漏形状”，是指每行输出奇数个符号；各行符号中心对齐；相邻两行符号数差2
//符号数先从大到小顺序递减到1，再从小到大顺序递增；首尾符号数相等
#include <stdio.h>
void printSign(const int, const char);
void printBlank(const int, const int);
int main(void)
{
    int sum, halfLine, temp, leftSignNum;
    char sign = '*';
    leftSignNum = sum = temp = 0;
    halfLine = 1;
    scanf("%d %c", &sum, &sign);    getchar();

    // 看看够打印几行
    while (1)
    {
        temp = 2 * halfLine * halfLine - 1;
        if (sum >= temp)
        {
            halfLine++;
            leftSignNum = sum - temp;
        }
        else
        {
            halfLine--;
            break;
        }
    }

    // 打印倒三角形
    for (int i = halfLine; i >= 1; i--)
    {
        printBlank(i, halfLine);
        printSign(i, sign);
    }

    // 打印正三角形
    for (int i = 2; i <= halfLine; i++)
    {
        printBlank(i, halfLine);
        printSign(i, sign);
    }

    // 没用掉的字符数
    printf("%d\n", leftSignNum);
    return 0;
}

// 打印给定行的特定个数的符号
void printSign(const int line, const char sign)
{
    int starnum = 2 * line - 1;
    for (int i = 0; i < starnum; i++)
        printf("%c", sign);
    printf("\n");
}

// 打印特定行的空格
void printBlank(const int n, const int line)
{
    int i = n;
    while (i < line)
    {
        printf(" ");
        i++;
    }
}
