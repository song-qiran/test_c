#define _CRT_SECURE_NO_WARNINGS 1
//编程一
//将数组A中的内容和数组B中的内容进行交换
//	#include<stdio.h>
//	#include<stdlib.h>
//	int main() 
//{
//	int i, a[5], b[5], t;  //提前设定好两个数组均为5个元素
//	printf("Please input the first array:");
//	for (i = 0; i < 5; i++)
//		scanf("%d", &a[i]);
//	printf("Please input the second array:");
//	for (i = 0; i < 5; i++)
//		scanf("%d", &b[i]);
//	for (i = 0; i < 5; i++)
//	{
//		t = a[i];
//		a[i] = b[i]; 
//		b[i] = t;  //利用第三变量t进行数组元素的交换
//	}
//	printf("交换后的数组:\n");
//	printf("Please input the first array:");
//	for (i = 0; i < 5; i++)
//		printf("%d ", a[i]);
//	printf("\n");
//	printf("Please input the second array:");
//	for (i = 0; i < 5; i++)
//		printf("%d ", b[i]);
//	printf("\n");
//	return 0;
//}
//编程二
//创建一个整形数组，完成对数组的操作
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//#include<stdio.h>
//#include<stdlib.h>
//void init(int arr[], int len) {
//	int i = 0;
//	for (i = 0; i < len; ++i) {
//		arr[i] = i;
//	}
//}
//void print(int arr[], int len) {
//	int i = 0;
//	for (i = 0; i < len; ++i) {
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//void reverse(int arr[], int len)
//{
//	//倒置思想
//	int i = 0;
//	int j = len - 1;
//	int tmp = 0;
//	while (i < j) {
//		tmp = arr[i];
//		arr[i] = arr[j];
//		arr[j] = tmp;
//		i++;
//		j--;
//	}
//}
//int main() 
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
//	int len = sizeof(arr) / sizeof(arr[0]);
//	init(arr, len);
//	print(arr, len);
//	reverse(arr, len);
//	print(arr, len);
//	print(arr, len);
//	return 0;
//}
//编程三
//实现对一整个数组的冒泡排序
//#include<stdio.h>
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//void bubble(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (arr[j] < arr[j + 1])//不满足前大后小
//			{
//				int temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);//数组长度
//	print_arr(arr, sz);//冒泡排序前
//	bubble(arr, sz);
//	print_arr(arr, sz);//冒泡排序后
//}
