﻿#define _CRT_SECURE_NO_WARNINGS 1
//测试代码
//#include<stdio.h>
//int main()
//{
//	int m, n;
//	printf("Enter m,n;");
//	scanf("%d%d", &m, &n); 
//	while (m != n) //1 
//	{ 
//		while(m>n) m=m-n; //2
//		while(n>m) n=n-m; //3 
//	}
//		printf("m=%d\n", m);
//		return 0;
//}

//测试代码
//#include <stdio.h> 
//int main() 
//{	char c;
//	int v0 = 0, v1 = 0, v2 = 0;
//do
//{
//	switch (c = getchar()) {
//	case'a':case'A':
//	case'e':case'E':
//	case'i':case'I':
//	case'o':case'O':
//	case'u':case'U':v1 += 1;
//	default: v0 += 1; v2 += 1;
//	}
//} while (c != '\n');
//printf("v0=%d,v1=%d,v2=%d\n", v0, v1, v2); return 0; }
//注意：遇到break才停止switch语句！！！
//
// 
// //编程->错误的集合
//集合 s 包含从 1 到 n 的整数。不幸的是，因为数据错误
//导致集合里面某一个数字复制了成了集合里面的另外一个数字的值，
//导致集合 丢失了一个数字 并且 有一个数字重复 。
//给定一个数组 nums 代表了集合 S 发生错误后的结果。
//请你找出重复出现的整数，再找到丢失的整数，将它们以数组的形式返回。
 

//小明同学最近开发了一个网站，在用户注册账户的时候，需要设置账户的密码，为了加强账户的安全性，
//小明对密码强度有一定要求：
//
//1. 密码只能由大写字母，小写字母，数字构成；
//
//2. 密码不能以数字开头；
//
//3. 密码中至少出现大写字母，小写字母和数字这三种字符类型中的两种；
//
//4. 密码长度至少为8
//
//现在小明受到了n个密码，他想请你写程序判断这些密码中哪些是合适的，哪些是不合法的。
#include<stdio.h>
#include<string.h>
int main()
{
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        for (int i = 0; i < n; i++)
        {
            char pwd[101] = { 0 };
            scanf("%s", pwd);
            int lower = 0, higher = 0, digit = 0, other = 0;
            if (pwd[0] >= '0' && pwd[0] <= '9')
            {
                printf("NO\n");
                continue;
            }//密码不能以数字开头
            if (strlen(pwd) < 8)
            {
                printf("NO\n");
                continue;
            }//密码长度至少为8
            for (int i = 0; pwd[i] != '\0'; i++)
            {
                if (pwd[i] >= '0' && pwd[i] <= '9')
                    digit++;//数字
                else if (pwd[i] >= 'a' && pwd[i] <= 'z')
                    lower++;//小写字母
                else if (pwd[i] >= 'A' && pwd[i] <= 'Z')
                    higher++;//大写字母
                else
                    other++;
            }
            if (other != 0)
            {
                printf("NO\n");
                continue;
            }// 密码只能由大写字母，小写字母，数字构成
          //大写，小写，数字，必须具有两种以上，而比较运算真则1，假则0
            if ((lower > 0) + (higher > 0) + (digit > 0) < 2)
            {
                printf("NO\n");
                continue;
            }//密码只有1种字符
            printf("YES\n");
        }
    }
    return 0;
}
