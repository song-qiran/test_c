#define _CRT_SECURE_NO_WARNINGS 1
//执行循环代码+ststic修饰局部变量的使用
//#include <stdio.h>
//int sum(int a)
//{
//    int c = 0;
//    static int b = 3;
//    c += 1;
//    b += 2;
//    return (a + b + c);
//}
//int main()
//{
//    int i;
//    int a = 2;
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d,", sum(a));
//    }
//}
//第二题 给定两个整数a和b (0 < a,b < 10,000)，计算a除以b的整数商和余数
//#include<stdio.h>
//int main()
//{
//    int a, b = 0;
//    scanf("%d %d", &a, &b);
//    int c, d = 0;
//    c = a / b;
//    d = a % b;
//    printf("%d %d", c, d);
//    return 0;
//}
//第三题 将一个四位数，反向输出。
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    scanf("%d", &a);
//    while (a)
//    {
//        printf("%d", a % 10);
//        a = a / 10;
//    }
//    return 0;
//}
//第四题 从键盘输入5个学生的成绩（整数），求他们的平均成绩（浮点数，保留一位小数）
//#include<stdio.h>
//int main()
//{
//    int score1, score2, score3, score4, score5 = 0;
//    scanf("%d %d %d %d %d", &score1, &score2, &score3, &score4, &score5);
//    float aver = 0;
//    aver = (score1 + score2 + score3 + score4 + score5) / 5.0;
//    printf("%.1f", aver);
//    return 0;
//}
//第五题 KiKi想知道从键盘输入的两个数的大小关系，请编程实现。
//输入描述：
//题目有多组输入数据，每一行输入两个整数（范围 - 231~231 - 1），用空格分隔。
//输出描述：
//针对每行输入，输出两个整数及其大小关系，数字和关系运算符之间没有空格，详见输入输出样例。
//#include<stdio.h>
//int main()
//{
//    int a, b = 0;
//    while (scanf("%d %d", &a, &b) != EOF)
//    {
//        if (a == b)
//        {
//            printf("%d=%d\n", a, b);
//        }
//        else if (a > b)
//        {
//            printf("%d>%d\n", a, b);
//        }
//        else printf("%d<%d\n", a, b);
//    }
//    return 0;
//}
//第六题
//给定秒数 seconds ，把秒转化成小时、分钟和秒。
//
//数据范围： 0 < seconds < 100000000\0 < seconds < 100000000
//	输入描述：
//	一行，包括一个整数，即给定的秒数。
//	输出描述：
//	一行，包含三个整数，依次为输入整数对应的小时数、分钟数和秒数（可能为零），中间用一个空格隔开。
//#include<stdio.h>
//    int main()
//{
//    int seconds = 0;
//    scanf("%d", &seconds);
//    int hour, min, sec = 0;
//    hour = seconds / 3600;
//    min = (seconds % 3600) / 60;
//    sec = (seconds % 3600) % 60;
//    printf("%d %d %d", hour, min, sec);
//    return 0;
//}