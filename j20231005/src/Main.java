import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 * Date: 2023-10-05
 * Time: 17:18
 */
public class Main {
    public static void main1(String[] args) {
        //实现输入指令方法
//        Scanner sc= new Scanner(System.in);
//        int a2=sc.nextInt();
//        System.out.println(a2);
        System.out.println("hello");
    }

    public static void main2(String[] args) {
        //输入小数
        Scanner a = new Scanner(System.in);
        double ad= a.nextDouble();
        System.out.println(ad);
    }

    public static void main3(String[] args) {
        //输入名字
        Scanner name=new Scanner(System.in);
//        String name1=name.next();  //空格后面就输入不出来了
        String name1 = name.nextLine(); //识别输出一行
        System.out.println(name1);
    }



    public static void main4(String[] args) {
        Scanner d = new Scanner(System.in);
        int d2 = d.nextInt();
        System.out.println(d2);
        Scanner name = new Scanner(System.in);
        String name2 = name.nextLine();
        System.out.println(name2);
    }



    public static void main5(String[] args) {
        //生成[0,100)的随机数,并判断输入的数字与随机数的大小关系
        Random random=new Random();
        int randroms = random.nextInt(100);//[0,100)
        System.out.println("请输入"+randroms);
        Scanner sc = new Scanner(System.in);
        while(true){
            int chance = sc.nextInt();
            if(chance>randroms){
                System.out.println("输入过大");
            } else if (chance<randroms) {
                System.out.println("输入过小");
            }
            else{
                System.out.println("输入正确");
                break;
            }
        }
    }


//判断素数
    public static void main6(String[] args) {
        int num = 10;
        int i = 0;
        for (i = 2; i < num; i++) {
            if(num % i == 0) {
                System.out.println("不是素数");
                break;
            }
        }
        if(num == i ) {
            System.out.println("是素数");
        }
    }


    //打印 1 - 100 之间所有的素数
    public static void main7(String[] args) {
        int i=0,j=0;
        for ( i = 1; i <= 100 ; i++) {
            for ( j = 2; j < i; j++) {
                if(i%j == 0) {
                    break;
                }
            }
            if(i==j){
                System.out.print(i+"  ");
            }
        }
    }


    //输出 1000 - 2000 之间所有的闰年
    public static void main8(String[] args) {
        for (int i = 1000; i <2001 ; i++) {
            if((i%400==0)||((i%4==0)&&(i%100!=0))){
                System.out.print(i+"  ");
            }
        }
    }

//编写程序数一下 1到 100 的所有整数中出现多少个数字9
public static void main9(String[] args) {
    int cnt=0;
    for (int i = 1; i < 101; i++) {
        if((i%10==9)||(i/10==9)){
            cnt++;
        }
    }
    System.out.println(cnt);
}


//输出n*n的乘法口诀表，n由用户输入。
public static void main10(String[] args) {
    Scanner scan = new Scanner(System.in);
    int n =scan.nextInt();
    for (int i = 1; i <= n ; i++) {
        for (int j = 1; j <= i ; j++) {
            System.out.print(j+"*"+i+"="+j*i+"  ");
        }
        System.out.println();
    }

}
//输出一个整数的每一位，如：123的每一位是3，2，1
    public static void main11(String[] args) {
        int num = 123;
        int a = num%10;
        int b = num/10%10;
        int c = num/100;
        System.out.println(num+"的每一位是"+a+","+b+","+c+" ");
    }

//模拟登录
    //编写代码模拟三次密码输入的场景。 最多能输入三次密码，密码正确，提示“登录成功”,密码错误，
    // 可以重新输 入，最多输入三次。三次均错，则提示退出程序
public static void main12(String[] args) {
        Scanner scan = new Scanner(System.in);
        int password = 500000;
        int trys = 0;
        int i=3;
        while(i>0)
        {
            i--;
            trys = scan.nextInt();
            if(trys==password){
                System.out.println("登录成功");
                break;
            }
            else{
                System.out.println("密码错误");
            }
        }
        if(trys!=password){
            System.out.println("退出程序");
        }
}


//获取一个数二进制序列中所有的偶数位和奇数位， 分别输出二进制序列
    public static void main13(String[] args) {
        int num;
        Scanner in = new Scanner(System.in);
        System.out.println("请输入一个整数:");
        num = in.nextInt();  //接收一个整数
        printOddEvenBinary(num);  //调用输出一个整数的二进制序列中的奇偶位序列的方法

    }
    //输出一个整数的二进制序列中的奇偶位序列的方法
    public static void printOddEvenBinary(int num){
        int odd = 0;
        int even = 0;
        int len1 = 0;
        int len2 = 0;
        int count = 1;
        //遍历num的每一个二进制位
        for (int i = num; i != 0; i >>= 1){
            if ((count & 1) == 1){  //若该二进制位为奇数位
                odd |= (i & 1) << len1;  //odd的值更新为i与1按位与左移len1位再与odd按位或
                len1++;
            }
            else{  //若该二进制位为偶数位
                even |= (i & 1) << len2;
                len2++;
            }
            count++;
        }

        System.out.println("奇数序列:");
        for (int i = len1 - 1; i >= 0; i--){  //遍历odd的各个二进制位
            System.out.print(odd >> i & 1);  //输出odd的各个二进制位
        }
        System.out.println();
        System.out.println("偶数序列:");
        for (int i = len2 - 1; i >= 0; i--){  //遍历even的各个二进制位
            System.out.print(even >> i & 1);  //遍历even的各个二进制位
        }
    }
//求出0-999之间的所有水仙花数
    public static void main14(String[] args) {
        for (int i = 0; i <1000 ; i++) {
            //求出位数
            int tmp = i;
            int num = 0;
            int sum=0;
            while(tmp!=0){
                num++;
                tmp/=10;
            }
            tmp = i;
            while(tmp!=0){
                sum+=Math.pow(tmp%10,num);
                tmp/=10;
            }
            if(sum==i){
                System.out.println(i);
            }
        }
    }
//求出一个数字二进制位中1的个数
    public static void main(String[] args) {
        int a = 15;
        int num = 0;
        for (int i = 0; i < 32; i++) {
            if((a & 1) == 1){
                num++;
            }
            a = a>>1;
        }
        System.out.println(num);
    }
}
