#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<vector>
#include<cstdio>
using namespace std;
#define Inf 0x3f3f3f3f
const int maxn = 1005;
vector<int> G[maxn];
int n, m;
int pre[maxn];
void init() {
	for (int i = 1; i <= n; i++)
		pre[i] = i;
}
int find(int x) {
	if (pre[x] == x)
		return x;
	else
		return pre[x] = find(pre[x]);
}
void merge(int x, int y) {
	int fx = find(x);
	int fy = find(y);
	if (fx != fy)
		pre[fx] = fy;
}
int main()
{
	cin >> n >> m;
	init();
	int a, b;
	for (int i = 1; i <= m; i++) {
		scanf("%d%d", &a, &b);
		merge(a, b);
		G[a].push_back(b);
		G[b].push_back(a);
	}
	int cnt = 0, num = 0;
	for (int i = 1; i <= n; i++) {
		if (G[i].size() % 2)
			cnt++;
		if (pre[i] == i)
			num++;
	}
	if (cnt == 0 && num == 1)
		cout << "1" << endl;
	else
		cout << "0" << endl;
	return 0;
}