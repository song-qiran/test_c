#define _CRT_SECURE_NO_WARNINGS 1
//数组指针的用法
void print1(int arr[10], int sz) //第一种方式->使用数组去接受实参传递过来的数组
{
	//创建的元素个数任意，因为实际传递过来的是数组首元素的地址
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]); //遍历打印数组
	}
	printf("\n");
}

void print1(int *arr, int sz) //第二种方式—>使用指针接受传递过来的数组元素首地址
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", *(arr+i));//遍历打印数组
	}
	printf("\n");
}

void print2(int(*p)[10], int sz)   //创建了数组指针->指针指的是数组的地址
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", (*p)[i]);//arr;
	}
	printf("\n");
}

void test1()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);    //求得数组的长度

	//print1(arr, sz);
	print2(&arr, sz);
}

void print3(int arr[3][5], int r, int c)    //实现打印二维数组
{		 
	int i = 0;
	for (i = 0; i < r; i++)
	{
		int j = 0;
		for (j = 0; j < c; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
}

void print4(int (*p)[5], int r, int c)  //传递的参数是数组指针、行数、列数
{
	int i = 0;
	for (i = 0; i < r; i++)
	{
		int j = 0;
		for (j = 0; j < c; j++)
		{
			printf("%d ", *(*(p + i) + j));
		}
		printf("\n");
	}
}

void test2()
{
	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
	print3(arr, 3, 5);
	print4(arr, 3, 5);
}

int main()
{
	//test1();
	test2();
	return 0;
}