#define _CRT_SECURE_NO_WARNINGS 1
// ģ��EXCEL����
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define MAX 100001

typedef struct {
    int  num;
    char name[10];
    int  score;
} Student;

Student student[MAX];

int cmpnum(const void* a, const void* b) {
    return ((Student*)a)->num - ((Student*)b)->num;
}

int cmpname(const void* a, const void* b) {
    int res = strcmp(((Student*)a)->name, ((Student*)b)->name);
    if (res == 0) {
        return ((Student*)a)->num - ((Student*)b)->num;
    }
    else if (res > 0) {
        return 1;
    }
    return 0;
}

int cmpscore(const void* a, const void* b) {
    int res = ((Student*)a)->score - ((Student*)b)->score;
    if (res == 0) {
        return ((Student*)a)->num - ((Student*)b)->num;
    }
    else if (res > 0) {
        return 1;
    }
    return 0;
}

int main() {
    int n, c;
    scanf("%d %d", &n, &c);
    for (int i = 0; i < n; i++) {
        scanf("%d %s %d", &student[i].num, student[i].name, &student[i].score);
    }
    if (c == 1) {
        qsort(student, n, sizeof(Student), cmpnum);
    }
    else if (c == 2) {
        qsort(student, n, sizeof(Student), cmpname);
    }
    else if (c == 3) {
        qsort(student, n, sizeof(Student), cmpscore);
    }
    for (int i = 0; i < n; i++) {
        printf("%06d %s %d\n", student[i].num, student[i].name, student[i].score);
    }
    return 0;
}
	