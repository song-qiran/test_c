#define _CRT_SECURE_NO_WARNINGS 1
////查找整数
//#include<stdio.h>
//int main()
//{
//    int n, x, k;
//    int flag = 0;
//    scanf("%d %d", &n, &x);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &k);
//        if (k == x)
//        {
//            printf("%d", i);
//            flag = 1;
//        }
//
//    }
//    if (flag == 0)
//    {
//        printf("Not Found");
//    }
//
//    return 0;
//}


//将数组中的数逆序存放
//#include<stdio.h>
//int main()
//{
//    int n, i;
//    scanf("%d", &n);
//    int arr[11] = { 0 };
//    for (i = n; i > 0; i--)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (i = 1; i <= n; i++)
//    {
//        printf("%d", arr[i]);
//        if (i == n)
//            continue;
//        printf(" ");
//    }
//
//    return 0;
//}


//选择法排序
#include<stdio.h>
int main()
{
    int n, i, j, k, temp, count;
    scanf("%d", &n);
    int a[n];
    for (i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }
    for (j = 0; j < n - 1; j++)
    {
        count = j;
        for (k = count + 1; k < n; k++)
        {
            if (a[k] > a[j]) { temp = a[j]; a[j] = a[k]; a[k] = temp; }
        }
    }
    for (i = 0; i < n; i++)
    {
        printf("%d", a[i]);
        if (i < n - 1) { printf(" "); }
    }

    return 0;
}
