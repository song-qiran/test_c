/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class Main {
    public static void main(String[] args) {
        //合并两个有序数组
        class Solution {
            public void merge(int[] nums1, int m, int[] nums2, int n) {
                int i = m-1;
                int j = n-1;
                int k = m+n-1;
                while(i>=0 && j>=0){
                    if(nums1[i]>nums2[j]){
                        nums1[k] = nums1[i];
                        k--;
                        i--;
                    }
                    else{
                        nums1[k] = nums2[j];
                        j--;
                        k--;
                    }
                }
                while(i>=0){
                    nums1[k] = nums1[i];
                    k--;
                    i--;
                }
                while(j>=0){
                    nums1[k] = nums2[j];
                    k--;
                    j--;
                }
            }
        }
    }


    public static void main3(String[] args) {
    //给你一个 非严格递增排列 的数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，
    // 返回删除后数组的新长度。元素的 相对顺序 应该保持 一致 。
    // 然后返回 nums 中唯一元素的个数。删除有序数组中的重复项.
//    nums = [1,1,2]
//    输出：2, nums = [1,2,_]

    /**
     * 使用双指针解决问题
     *设置左指针 left     右指针 right
     * right 在left 后面一个的位置
     * 因为是递增序列,所以如果重复元素则也是相邻位置的元素
     * 所以让右指针指向的元素与左指针指向的元素比较
     * 若相等,则右指针++  不相等,则右指针指向的元素赋值到左指针指向的元素的下一个位置
     * 重点在<原地>
     *     */

        class Solution {
            public int removeDuplicates(int[] nums) {
                if(nums == null || nums.length == 0)  return 0;
                int left = 0;
                int right = 1;
                while(right<nums.length){
                    if(nums[right] == nums[left]){
                        right++;
                    }
                    else{
                        nums[left+1] = nums[right];
                        left++;
                        right++;
                    }
                }
                return left+1;
            }
        }
    }
    public static void main2(String[] args) {
        //leetcode  27 移除数组
        /**
         * 思路->
         * 使用双指针做法
         * 右指针和左指针分别从数组第一个位置开始计数,右指针先走.
         * 当右指针所指向的数组元素等于val时候,右指针继续右移,左指针不动.
         * 当右指针所指向的数组元素不等于val时候,右指针指向的元素原地赋值给左指针指向的元素
         * 右指针和左指针同时右移.
         * 本题主要重点在<原地>位置上移动与删除
         */
        //给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，
        // 并返回移除后数组的新长度。
        //不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
        //元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
        class Solution {
            public int removeElement(int[] nums, int val) {
                int left = 0;
                int right = 0;
                while(right < nums.length){
                    if(nums[right] != val){
                        nums[left] = nums[right];
                        right++;
                        left++;
                    }
                    else{
                        right++;
                    }
                }
                return left;
            }
        }
    }
    public static void main1(String[] args) {
        //leetcode  189  轮转数组
        // 给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。
        //输入: nums = [1,2,3,4,5,6,7], k = 3
        //输出: [5,6,7,1,2,3,4]
/**   思路--
        直接创建一个临时拷贝数组,遍历数组,每个数组元素的值被拷贝成(i+k)%length的下标位置
        **/

        class Solution {
            public void rotate(int[] nums, int k) {
                int[] tmp = new int[nums.length];
                for(int i = 0 ;i < nums.length ;i++){
                    tmp[(i+k)%nums.length] = nums[i];
                }
                System.arraycopy(tmp,0,nums,0,nums.length);
            }
        }

    }
}