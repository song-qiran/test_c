#define _CRT_SECURE_NO_WARNINGS 1
//单链表的冒泡排序
//=============冒泡排序====================
void bubbleSort(list mylist)
{
    if ((mylist->next == NULL) || (mylist->next->next == NULL))
    {
        return;
    }

    node* head, * pre, * cur, * next, * end, * temp;
    head = mylist;
    end = NULL;
    //从链表头开始将较大值往后沉
    while (head->next != end)
    {
        for (pre = head, cur = pre->next, next = cur->next; next != end; pre = pre->next, cur = cur->next, next = next->next)
        {
            //相邻的节点比较
            if (cur->val > next->val)
            {
                cur->next = next->next;
                pre->next = next;
                next->next = cur;
                temp = next;
                next = cur;
                cur = temp;
            }
        }
        end = cur;
    }
}
//=============冒泡排序====================