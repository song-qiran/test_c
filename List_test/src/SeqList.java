import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class SeqList {
    private int[] elem;
    private int usedSize;  //记录有多少个有效数字
    private static final int DEFUALT_CAPACITY =5;
    //--------------------------------------------


    //打印顺序表

    public void display(){
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(this.elem[i]+" ");
        }
        System.out.println();
    }


    //--------------------------------------------
    //新增元素，默认在数组最后新增
    public  void add(int data){
        //首先要判断满的情况
        if(isFull()){
            resize();
        }
        this.elem[usedSize] = data;
        usedSize++;
    }


    //---------------------------------------------
    //判断数组是否已满
    public boolean isFull(){
        return usedSize == elem.length;
    }


    //------------------------------------------------
    // 判定是否包含某个元素
    public boolean contains(int toFind)
    {
        for (int i = 0; i < this.usedSize; i++) {
            if(elem[i] == toFind){
                return true;
            }
        }
       return false;
    }


    //------------------------------------------------
    // 查找某个元素对应的位置  --- 下标
    public int indexOf(int toFind)
    {
        for (int i = 0; i < this.usedSize; i++) {
            if(elem[i] == toFind){
                return  i;
            }
        }
        return -1;
    }


    //------------------------------------------------
    // 获取 pos 位置的元素
    public int get(int pos)
    {
        if(!checkPos(pos) ){
            throw new PosOutBoundsException("get 获取数据时，位置不合法！");
        }
        return elem[pos];
    }


    //------------------------------------------------
    // 在 pos 位置新增元素
    public void add(int pos, int data)
    {
        if(pos<0 || pos>usedSize){
            throw new PosOutBoundsException("add 元素的时候，pos位置不合法") ;
        }
        if(isFull()){
            resize();
        }
        //挪数据
        for (int i = this.usedSize-1; i >=pos ; i--) {
            this.elem[i+1] =this.elem[i];
        }
        //存数据
        this.elem[pos] = data;
        this.usedSize++;
    }



    //------------------------------------------------
    // 给 pos 位置的元素设为 value------------更新
    public void set(int pos, int value)
    {
        if(!checkPos(pos) ){
            throw new PosOutBoundsException("set 获取数据时，位置不合法！");
        }
        this.elem[pos] = value;
    }


    //------------------------------------------------
    //删除第一次出现的关键字key
    public void remove(int toRemove) {
       if(isEmpty()){
           return;
       }
       int index = indexOf(toRemove);
       if(index == -1){
           return;  //没有要删除的数字
       }
        for (int i = index; i < usedSize-1; i++) {
            this.elem[i] = this.elem[i+1];
        }
        usedSize--;
    }


    //------------------------------------------------
    // 获取顺序表长度
    public int size()
    {   return this.usedSize;
    }


    //------------------------------------------------
    // 清空顺序表
    public void clear() {
//        for (int i = 0; i < usedSize; i++) {
//            this.elem[i] = null;
//        }
        usedSize = 0;
    }


    //------------------------------------------------
    //判断pos是否合法
    private boolean checkPos(int pos){
        if(pos >= this.usedSize || pos < 0){
            return  true;
        }
            else{
                return  false;
            }

    }


    //扩容
    private  void resize(){
            elem = Arrays.copyOf(elem,2*elem.length);
    }

    private  boolean isEmpty(){
        return  usedSize == 0;
    }
    public SeqList(){
        this.elem = new int[DEFUALT_CAPACITY];
    }
}
