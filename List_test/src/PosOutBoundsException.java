/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class PosOutBoundsException extends  RuntimeException{
    public PosOutBoundsException() {
    }

    public PosOutBoundsException(String message) {
        super(message);
    }
}
