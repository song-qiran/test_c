#define _CRT_SECURE_NO_WARNINGS 1
//全局友元函数
#include<iostream>
using namespace std;
#include<string>
//建筑物类
class Building
{
    //全局友元函数
    friend void goodGay(Building* building);

public:
    Buliding()
    {
        m_SittingRoom = "客厅";
        m_BedRoom = "卧室";
    }

public:
    string m_SittingRoom;

private:
    string m_BedRoom;
};

//全局函数
void goodGay(Building* building)
{
    cout << "Public" << building->m_SittingRoom << endl;

    //成为友元函数后可以访问私有变量
    cout << "Private" << building->m_BedRoom << endl;
}

void test01()
{
    Building building;
    goodGay(&building);
}
int main()
{
    test01();

    return 0;
}

