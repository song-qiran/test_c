#define _CRT_SECURE_NO_WARNINGS 1
//求幂级数展开的部分和
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
double fun(double x, double n)//求数列每一项的值
{
	double ret = 0;
	double  i = 0;
	double num = 1;
	//求分母：即阶乘
	for (i = 1; i <= n; i++)
	{
		num = num * i;
	}
	ret = pow(x, n) / num;//数列每一项的值
	//printf("ret=%.4f,", ret);
	return ret;
}
int main()
{
	double x = 0;
	double sum = 0;
	double n = 0;
	scanf("%lf", &x);
	do
	{
		sum = sum + fun(x, n);
		//printf("n=%lf,sum=%.4f\n",n, sum);//加入这些输出语句都是便于查看各项值，方便调试所用
		n++;

	} while (fabs(fun(x, n)) >= 0.00001);
	sum = sum + fun(x, n);//最后一项的绝对值必须小于0.00001，而上面的循环sum的值没有把最后一项包含进去，所以此处要添加这一句
	//printf("n=%lf\n", n);
	printf("%.4f\n", sum);
	system("pause");
	return 0;
}
//输出整数各位数字
//三种方法
//法一
//将该整数看成若干个数字字符组成的字符串，将字符串中的所有数字字符逐个输出。
# include <stdio.h>

int main()
{
	char c;

	do
	{
		scanf("%c", &c);

		if (!(c >= '0' && c <= '9'))    //利用最后输入的回车键也会被看做字符被scanf读取的特点跳出循环。 
			break;

		printf("%c ", c);

	} while (c >= '0' && c <= '9');

	return 0;
}


//法二
//引用数组，先从最低位开始取余，最后将数组倒着输出。
# include <stdio.h>

int main()
{
	int i;
	int j;
	int a[50] = { 0 };
	int n;
	scanf(" %d", &n);

	if (0 == n)
	{
		printf("0 \n");
		return 0;
	}

	for (i = 1; n; ++i)
	{
		a[i] = n % 10;
		n /= 10;
	}

	--i;

	while (i)
	{
		printf("%d ", a[i]);
		--i;
	}

	return 0;
}

//法三
//先测出该整数的总位数，再从最高位开始取余。
# include <stdio.h>

int main()
{
	int temp;
	int pow = 1;
	int i = 1;
	int n;
	scanf(" %d", &n);

	temp = n;
	while (temp / 10)
	{
		temp /= 10;
		pow *= 10;
		++i;
	}

	while (i)
	{
		printf("%d ", n / pow);
		n %= pow;
		pow /= 10;
		--i;
	}

	printf("\n");

	return 0;
}


