import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 */
public class Main {

//求字符串中第一个唯一字符
    public static void main(String[] args) {
        class Solution {
            public int firstUniqChar(String s) {
                int[] count = new int[26];
                for(int i=0;i<s.length();i++){
                    char ch = s.charAt(i);
                    count[ch-'a']++;
                }
                for(int i=0;i<s.length();i++){
                    char ch = s.charAt(i);
                    if(count[ch-'a'] == 1){
                        return i;
                    }
                }
                return -1;
            }
        }
    }

    public static void main6(String[] args) {
        StringBuffer stringBuffer = new StringBuffer("hello");
        stringBuffer.append("123");
        StringBuilder stringBuilder = new StringBuilder("dkwod");
        stringBuilder.append("123");
        System.out.println(stringBuilder);
        System.out.println("===================");
        System.out.println(stringBuffer);
    }
    public static void main5(String[] args) {
        String s = "hello";
        StringBuffer stringBuffer = new StringBuffer(s);//使用StingBuffer.insert()函数插入某一个字符串
        stringBuffer.insert(1,"abccc");
        System.out.println(stringBuffer);
        stringBuffer.delete(1,3);//删除某一段字符串   [1,3)
        System.out.println(stringBuffer);
        stringBuffer.reverse();//反转字符串
        System.out.println(stringBuffer);
    }
    //改写leetcode问题代码
    class Solution {
        public int countSegments(String s) {
            if(s==null){
                return 0;
            }
            if(s.length()==0){
                return 0;
            }
            String[] scc = s.split(" ");
            int count = 0;
            for(String ss:scc){
                if(ss.length()!=0){
                    count++;
                }
            }
            return count;
        }
    }

    //转换成小写字母
    //给你一个字符串 s ，将该字符串中的大写字母转换成相同的小写字母，返回新的字符串。
    //示例 1：
    //输入：s = "Hello"
    //输出："hello"
    //示例 2：
    //输入：s = "here"
    //输出："here"
    //示例 3：
    //输入：s = "LOVELY"
    //输出："lovely"
    public static void main4(String[] args) {
     Scanner scanner = new Scanner(System.in);
     String sc = scanner.nextLine();
     String ret = sc.toLowerCase();
     System.out.println(ret);
    }




    public static void main3(String[] args) {
        String sc = "hellp wpeld";
        String [] scc = sc.split(" ");
        for (String scccc: scc) {
            System.out.println(scccc);
        }
    }
//符串中的单词数
//    统计字符串中的单词个数，这里的单词指的是连续的不是空格的字符。
//    请注意，你可以假定字符串里不包括任何不可打印的字符。
//    示例:
//    输入: "Hello, my name is John"
//    输出: 5
//    解释: 这里的单词是指连续的不是空格的字符，所以 "Hello," 算作 1 个单词。
    public static void main2(String[] args) {
        String sc = new String();
        Scanner scanner = new Scanner(System.in);
        sc = scanner.nextLine();
        String[] split = sc.split(" ");
        System.out.println(split.length);
    }
    public static void main1(String[] args) {
        //String类练习
        String s1="abc"+"def";//1
        String s2=new String(s1);//2
        if(s1.equals(s2))//3
            System.out.println(".equals succeeded");//4
        if(s1==s2)//5
            System.out.println("==succeeded");//6

    }
}