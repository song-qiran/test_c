#define _CRT_SECURE_NO_WARNINGS 1
//第一行包含两个正整数n, m，用空格分隔。n表示第二行第一个升序序列中数字的个数，
//m表示第三行第二个升序序列中数字的个数。
//第二行包含n个整数，用空格分隔。
//第三行包含m个整数，用空格分隔。
int main()
{
    int n = 0;
    int m = 0;
    int arr1[1000] = { 0 };
    int arr2[1000] = { 0 };
    //输入n和m
    scanf("%d %d", &n, &m);
    int i = 0;
    int j = 0;
    //输入两个升序序列
    for (i = 0; i < n; i++)
    {
        scanf("%d", &arr1[i]);
    }
    for (i = 0; i < m; i++)
    {
        scanf("%d", &arr2[i]);
    }
    //合并有序序列并输出
    i = 0; j = 0;
    while (i < n && j < m)
    {
        if (arr1[i] < arr2[j])
        {
            printf("%d ", arr1[i]);
            i++;
        }
        else
        {
            printf("%d ", arr2[j]);
            j++;
        }
    }

    //判断尚未遍历完的数组是否需要打印输出
    if (i == n && j < m)
        for (; j < m; j++)
            printf("%d ", arr2[j]);
    else
        for (; i < n; i++)
            printf("%d ", arr1[i]);
    return 0;
}

