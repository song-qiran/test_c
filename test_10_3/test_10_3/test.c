#define _CRT_SECURE_NO_WARNINGS 1
//使用函数计算两个复数之积
//#include<stdio.h> 
//
//double result_real, result_imag;
//void complex_prod(double x1, double y1, double x2, double y2);
//
//int main(void)
//{
//    double imag1, imag2, real1, real2;
//
//    scanf("%lf %lf", &real1, &imag1);
//    scanf("%lf %lf", &real2, &imag2);
//    complex_prod(real1, imag1, real2, imag2);
//    printf("product of complex is (%f)+(%f)i\n", result_real, result_imag);
//
//    return 0;
//}
//double result_real, result_imag;
//void complex_prod(double x1, double y1, double x2, double y2)
//{
//    result_real = x1 * x2 - y1 * y2;
//    result_imag = x1 * y2 + x2 * y1;
//}

//利用辗转相除法求解两个数的最大公约数
//#include <stdio.h>
//
//int gcd(int x, int y);
//
//int main()
//{
//    int x, y;
//
//    scanf("%d %d", &x, &y);
//    printf("%d\n", gcd(x, y));
//
//    return 0;
//}
//int gcd(int x, int y)
//{
//    int d = 1;
//    int c = 0;
//    while (d)
//    {
//        c = x / y;
//        d = x % y;
//        x = y;
//        y = d;
//    }
//    return x;
//}
//

//使用函数统计指定数字的个数
#include <stdio.h>

int CountDigit(int number, int digit);

int main()
{
    int number, digit;

    scanf("%d %d", &number, &digit);
    printf("Number of digit %d in %d: %d\n", digit, number, CountDigit(number, digit));

    return 0;
}
int CountDigit(int number, int digit)
{
	int count = 0, remainder;

	if (number < 0) {
		number *= -1;
	}
	while (number > 10) {
		remainder = number % 10;
		number = number / 10;
		if (remainder == digit) {
			count = count + 1;
		}
	}
	if (number == digit) {
		count = count + 1;
	}

	return count;
}