#define _CRT_SECURE_NO_WARNINGS 1
//测试代码
//#include <stdio.h> 
//int main() 
//{
//	char ch;
//	while ((ch = getchar()) != '\n') 
//{
//	if (ch % 2 != 0 && (ch >= 'a' && ch <= 'z'))
//		ch = ch - 'a' + 'A';
//	putchar(ch);
//}
//	printf("\n");
//	return 0; 
//}
//
//编程一->数字在升序数组中出现的次数
//给定一个长度为 n 的非降序数组和一个非负数整数 k ，要求统计 k 在数组中出现的次数
//使用二分查找法
//#include<stdio.h>
//int find_t(int* a, int key, int sz)
//{
//	int left = 0, right = sz - 1;
//	int mid = 0;
//	while (left <= right)//循环条件是左边小于等于右边，当左边大于右边，不用再循环；
//	{
//		mid = (right - left) / 2 + left;//找到中间值（此做法是为了避免溢出）
//		if (a[mid] < key)//当要找到的key在中间值的右边时
//		{
//			left = mid + 1;//左边界向前进一位，压缩寻找范围
//		}
//		else if (a[mid] > key)//当要找到的key在中间值的左边时
//		{
//			right = mid - 1;//右边界向后退一位，压缩寻找范围
//		}
//		else
//			return mid;//当key刚好等于中间值，返回该值
//	}
//	return -1;//遍历整个数组后，都没有找到合适值，说明该数组中没有要寻找的key值，返回-1；
//}
//int main()
//{
//	int k = 0;
//	int arr[] = { 3,7,8,16,25,34,43,52,61,70 };
//	while (1)
//	{
//		scanf("%d", &k);
//		int sz = sizeof(arr) / sizeof(arr[0]);
//		if (find_t(arr, k, sz) != -1)
//			printf("exist the number is:%d\n", find_t(arr, k, sz));
//		else
//			printf("no exist!\n");
//	}
//	return 0;
//}

//编程二  ->整数转换。编写一个函数，确定需要改变几个位才能将整数 A 转成整数 B 。
int f3(int n) {
    int num = 0;
    unsigned int flag = 1;
    while (flag) {
        if (n & flag)
            num++;
        flag = flag << 1;
    }
    return num;
}

int convertInteger(int A, int B) {
    int C = A ^ B;//异或
    int t = f3(C);
    return t;
}
