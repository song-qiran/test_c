#define _CRT_SECURE_NO_WARNINGS 1
//数组循环左移
#include<stdio.h>
int main()
{
	int n, m, i;
	scanf("%d%d", &n, &m);//获取n和m的值
	if (m > n)
		m = m % n;//去掉多余无意义的左移次数
	int a[n];
	for (i = n - m; i < n; i++)//先从左移后的位置开始存
		scanf("%d", &a[i]);
	for (i = 0; i < n - m; i++)//再从a[0]往后存
		scanf("%d", &a[i]);
	for (i = 0; i < n; i++)//输出数组中的各个元素
	{
		printf("%d", a[i]);
		if (i != n - 1)//这样是为了让结尾没有空格
			printf(" ");
	}
	return 0;
}
