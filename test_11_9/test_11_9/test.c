#define _CRT_SECURE_NO_WARNINGS 1
//有理数比较
//#include<stdio.h>
//int main()
//{
//    int a1, a2, a3, a4;
//    double x1, x2;
//    scanf("%d/%d %d/%d", &a1, &a2, &a3, &a4);
//    x1 = a1 * 1.0 / a2;
//    x2 = a3 * 1.0 / a4;
//    if (x1 > x2)
//        printf("%d/%d > %d/%d", a1, a2, a3, a4);
//    else if (x1 == x2)
//        printf("%d/%d = %d/%d", a1, a2, a3, a4);
//    else if (x1 < x2)
//        printf("%d/%d < %d/%d", a1, a2, a3, a4);
//    return 0;
//}



//平面向量加法
//#include<stdio.h>
//#include<math.h>    //使用fabs函数  ->该函数返回的是x的绝对值
//int main()
//{
//    double x1, y1, x2, y2;
//    scanf("%lf %lf %lf %lf", &x1, &y1, &x2, &y2);
//    double x, y;
//    x = x1 + x2;
//    y = y1 + y2;
//    if (fabs(x) < 0.05)      //小数点后多位，检查舍入和 - 0
//        x = 0.0;
//    if (fabs(y) < 0.05)
//        y = 0.0;
//    printf("(%.1lf, %.1lf)", x, y);
//    return 0;
//}


//找出总分最高的学生
#include<stdio.h>
struct student {
    char num[10];
    char name[12];
    int score[3];
    int total;
}s[15];
int main()
{
    int i, n, j, flag = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%s %s %d %d %d", &s[i].num, &s[i].name, &s[i].score[0], &s[i].score[1], &s[i].score[2]);
        s[i].total = s[i].score[0] + s[i].score[1] + s[i].score[2];
    }
    for (i = 0; i < n; i++)
    {
        if (s[i].total > s[flag].total)    flag = i;
    }
    printf("%s %s %d", s[flag].name, s[flag].num, s[flag].total);

    return 0;
}







