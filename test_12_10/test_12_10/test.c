#define _CRT_SECURE_NO_WARNINGS 1
//两个有序链表序列的合并

List Merge(List L1, List L2) {
	List L, p, q, r;
	L = (List)malloc(sizeof(struct Node));	//新建一个结点
	p = L1->Next;	//p指向L1最小值
	q = L2->Next;	//q指向L2最小值
	L->Next = NULL;
	r = L;		//r指向L
	while (p != NULL && q != NULL)	//p、q都不空，选取p、q所指结点较小者插入L的尾部
	{
		if (p->Data <= q->Data) {
			r->Next = p;
			p = p->Next;
			r = r->Next;
		}
		else {
			r->Next = q;
			q = q->Next;
			r = r->Next;
		}
	}
	if (p != NULL) r->Next = p;
	if (q != NULL) r->Next = q;
	L1->Next = NULL;
	L2->Next = NULL;
	return L;
}
#include <stdio.h>
#include <stdlib.h>

typedef int ElementType;
typedef struct Node* PtrToNode;
struct Node {
	ElementType Data;
	PtrToNode   Next;
};
typedef PtrToNode List;

List Read(); /* 细节在此不表 */
void Print(List L); /* 细节在此不表；空链表将输出NULL */

List Merge(List L1, List L2);

int main()
{
	List L1, L2, L;
	L1 = Read();
	L2 = Read();
	L = Merge(L1, L2);
	Print(L);
	Print(L1);
	Print(L2);
	return 0;
}

/* 你的代码将被嵌在这里 */
