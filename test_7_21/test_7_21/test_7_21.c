#define _CRT_SECURE_NO_WARNINGS 1
//计算1到10阶乘的和
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int m = 0;
//	int ret = 1;
//	int add = 0;
//	for (m = 1; m <= 10; m++)
//	{
//		for (i = 1; i <= m; i++)
//		{
//			ret *= i;
//		}
//		add += ret;
//	}
//	printf("%d", add);
//	return 0;
//}


//判断输出结果
//#include <stdio.h>
//int main()
//{
//	int a = 0, b = 0;
//	for (a = 1, b = 1; a <= 100; a++)
//	{
//		if (b >= 20) break;
//		if (b % 3 == 1)
//		{
//			b = b + 3;
//			continue;
//		}
//		b = b - 5;
//	}
//	printf("%d\n", a);
//	return 0;
//}
//程序的输出结果是   8

//编程二
//计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值，打印出结果
//#include<stdio.h>
//int main()
//{
//	int i, j;
//	double sum;
//	double sum1 = 0.0;
//	double sum2 = 0.0;
//	//求小于100的分母为奇数分子为1数之和
//	for (i = 1; i < 100; i = i + 2)
//	{
//		sum1 = sum1 + 1.0 / i;
//	}
//	//求小于100的分母为偶数分子为1数之和
//	for (j = 2; j < 101; j = j + 2)
//	{
//		sum2 = sum2 + 1.0 / j;
//	}
//	sum = sum1 - sum2;
//	printf("sum=%f\n", sum);
//	return 0;
//}
//编程三
//编写程序数一下 1到 100 的所有整数中出现多少个数字9
//#include<stdio.h>
//int main()
//{
//	int count = 0;
//	int i = 0;
//	for (i = 1; i < 101; i++)
//	{
//		if (i % 10 == 9)
//			count++;
//	}
//	printf("出现了%d个数字9", count);
//	return 0;
//}
//
//编程四  猜数字游戏
//#include<stdio.h>
//#include <stdlib.h>
//#include <time.h>
//void menu()
//{
//	printf("*********************************\n");
//	printf("**********  1. play  ************\n");
//	printf("**********  0. exit  ************\n");
//	printf("*********************************\n");
//}
//void game()
//{
//	int guess = 0;
//	int ret = rand()%100+1;
//	while (1)
//	{
//		printf("请猜数字:>");
//		scanf("%d", &guess);
//		if (guess < ret)
//		{
//			printf("猜小了\n");
//		}
//		else if (guess > ret)
//		{
//			printf("猜大了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//}
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择!\n");
//			break;
//		}
//
//	} while (input);
//
//	return 0;
//}
//

//编程
//实现二分查找
#include<stdio.h>
int main()
{
	int arr[] = { 2,4,6,8,10,12,14,16,18,20 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz-1;
	int k = 0;
	scanf("%d", &k);

	while (left<=right)
	{
		//int mid = (left + right) / 2;
		int mid = left + (right - left) / 2;
		if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else
		{
			printf("找到了，下标是:%d\n", mid);
			break;
		}
	}
	if (left > right)
		printf("找不到\n");

	return 0;
}

