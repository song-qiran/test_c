#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{
//	int i = 0, a = 1, b = 2, c = 3, d = 4;
//	i = a++ && ++b && ++d;
//	printf("%d %d %d %d", a, b, c, d);
//	return 0;
//}
//输出结果代码
//#include <stdio.h>
//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;
//	b = ++c, c++, ++a, a++;
//	b += a++ + c;
//	printf("a = %d b = %d c = %d\n:", a, b, c);
//	return 0;
//}
//交换两个变量，不允许创建临时变量
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a=%d  b=%d", a, b);
//	return 0;
//}
//写一个函数返回参数二进制中 1 的个数
//#include<stdio.h>
//int countk(int num)
//{
//    int count = 0;
//    while (num)
//    {
//        if (num & 1) {
//            ++count;
//        }
//        num >>= 1;
//    }
//    return count;
//}
//int main()
//{
//    int num = 0;
//    scanf("%d", &num);
//    int k=countk(num);
//    printf("%d个1", k);
//    return 0;
//}
//求两个数二进制中不同位的个数
#include <stdio.h>
int main()
{
	int m, n, num = 0;
	printf("请输入两个整数：\n");
	scanf("%d %d", &m, &n);
	//位异或操作，仅有对应位相异才为1，其他全为0
	//只要判断出来操作之后的数
	//其二进制数中有几个1即可
	num = m ^ n;
	int count = 0;
	for (int i = 0; i < 33; i++)
	{
		if (num & 1 == 1)
		{
			count++;
		}
		num = num >> 1;
	}
	printf("%d", count);
	return 0;
}
