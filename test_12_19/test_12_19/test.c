#define _CRT_SECURE_NO_WARNINGS 1
// 整型关键字的散列映射
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int i, n, p, x, k;
    scanf("%d %d", &n, &p);
    int* s = (int*)malloc(sizeof(int) * p);
    memset(s, 0, sizeof(int) * p);
    for (i = 0; i < n; i++) {
        scanf("%d", &x);
        k = x % p;
        if (s[k] == 0) {
            s[k] = x;
        }
        else {
            while (s[k] != 0 && s[k] != x) {

                k = (k + 1) % p;
            }
            if (s[k] != x) {
                s[k] = x;
            }

        }
        if (i == 0) {
            printf("%d", k);
        }
        else {
            printf(" %d", k);
        }
    }

    return 0;
}