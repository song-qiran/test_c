#define _CRT_SECURE_NO_WARNINGS 1

//构造哈夫曼树
void createHuffmanTree(HuffmanTree & HT, int* w, int n) {
	if (n <= 1)
		return;
	//对树赋初值
	for (int i = 1; i <= n; i++) {//HT前n个分量存储叶子节点，他们均带有权值
		HT[i].weight = w[i];
		HT[i].lchild = 0;
		HT[i].parent = 0;
		HT[i].rchild = 0;
	}
	for (int i = n + 1; i <= M; i++) {//HT后m-n个分量存储中间结点，最后一个分量显然是整棵树的根节点
		HT[i].weight = 0;
		HT[i].lchild = 0;
		HT[i].parent = 0;
		HT[i].rchild = 0;
	}
	//开始构建哈夫曼树，即创建HT的后m-n个结点的过程，直至创建出根节点。用哈夫曼算法
	for (int i = n + 1; i <= M; i++) {
		int s1, s2;
		select(HT, i - 1, s1, s2);//在HT[1...i-1]里选择parent为0的且权值最小的2结点，其序号分别为s1,s2，parent不为0说明该结点已经参与构造了，故不许再考虑
		HT[s1].parent = i;
		HT[s2].parent = i;
		HT[i].lchild = s1;
		HT[i].rchild = s2;
		HT[i].weight = HT[s1].weight + HT[s2].weight;
	}
}