#define _CRT_SECURE_NO_WARNINGS 1
//弹球距离	
double dist(double h, double p)
{
    double sum = h;
    double t = p;
    double tt = t * h;
    while (tt > TOL)
    {
        sum += 2 * tt;
        tt = tt * p;
    }
    return sum;
}
#include <stdio.h>
#define TOL 1E-3

double dist(double h, double p);

int main()
{
    double h, p, d;
    scanf("%lf %lf", &h, &p);
    d = dist(h, p);
    printf("%.6f\n", d);
    return 0;
}

/* 你的代码将被嵌在这里 */
