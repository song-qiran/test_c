#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define Name_Length 20
#define Sentence_Lenth 120

struct Node {
	//	自己的名字
	char Name[Name_Length];
	//	父亲的名字
	char Father[Name_Length];
	//	自己名字前空白字符的个数
	int BlankCount;
} People[Sentence_Lenth];

int main() {
	int n, m;
	char LocalName[Sentence_Lenth];
	scanf("%d %d", &n, &m);
	getchar();
	for (int i = 0; i < n; i++) {
		People[i].BlankCount = 0;
		gets(LocalName);
		int LName_Len = strlen(LocalName);
		for (int j = 0; j < LName_Len; j++) {
			if (LocalName[j] == ' ') {
				People[i].BlankCount++;
			}
			else {
				strcpy(People[i].Name, LocalName + j);
				break;
			}
		}
		//	如果是没有空白字符的话，父亲就假设为NULL
		if (!People[i].BlankCount) {
			strcpy(People[i].Father, "NULL");
			//	反之
		}
		else {
			for (int k = i - 1; k >= 0; k--) {
				//	寻找第一个人的空白数比自己少的名字，就是父亲的名字
				if (People[i].BlankCount > People[k].BlankCount) {
					strcpy(People[i].Father, People[k].Name);
					break;
				}
			}
		}
	}
	char FrontName[Name_Length], Judge[Name_Length], BehindName[Name_Length],
		Temp[Name_Length];
	char LocalName1[Name_Length], LocalName2[Name_Length];
	for (int i = 0; i < m; i++) {
		scanf("%s %s %s %s %s %s", FrontName, Temp, Temp, Judge, Temp,
			BehindName);
		getchar();
		//X is a child of Y
		if (Judge[0] == 'c') {
			for (int k = 0; k < n; k++) {
				if (!strcmp(People[k].Name, FrontName)) {
					if (!strcmp(People[k].Father, BehindName)) {
						printf("True\n");
					}
					else {
						printf("False\n");
					}
					break;
				}
			}
			//X is the parent of Y
		}
		else if (Judge[0] == 'p') {
			for (int k = 0; k < n; k++) {
				if (!strcmp(People[k].Name, BehindName)) {
					if (!strcmp(People[k].Father, FrontName)) {
						printf("True\n");
					}
					else {
						printf("False\n");
					}
					break;
				}
			}
			// X is a sibling of Y
		}
		else if (Judge[0] == 's') {
			for (int k = 0; k < n; k++) {
				if (!strcmp(People[k].Name, FrontName)) {
					strcpy(LocalName1, People[k].Father);
				}
				if (!strcmp(People[k].Name, BehindName)) {
					strcpy(LocalName2, People[k].Father);
				}
			}
			if (!strcmp(LocalName1, LocalName2)) {
				printf("True\n");
			}
			else {
				printf("False\n");
			}
			//X is a descendant of Y
		}
		else if (Judge[0] == 'd') {
			for (int k = 0; k < n; k++) {
				if (!strcmp(People[k].Name, FrontName)) {
					strcpy(LocalName1, People[k].Father);
				}
			}
			while (strcmp(LocalName1, BehindName) && strcmp(LocalName1, "NULL")) {
				for (int k = 0; k < n; k++) {
					if (!strcmp(People[k].Name, LocalName1)) {
						strcpy(LocalName1, People[k].Father);
					}
				}
			}
			if (!strcmp(LocalName1, "NULL")) {
				printf("False\n");
			}
			else {
				printf("True\n");
			}
			//X is an ancestor of Y
		}
		else if (Judge[0] == 'a') {
			for (int k = 0; k < n; k++) {
				if (!strcmp(People[k].Name, BehindName)) {
					strcpy(LocalName1, People[k].Father);
				}
			}
			while (strcmp(LocalName1, FrontName) && strcmp(LocalName1, "NULL")) {
				for (int k = 0; k < n; k++) {
					if (!strcmp(People[k].Name, LocalName1)) {
						strcpy(LocalName1, People[k].Father);
					}
				}
			}
			if (!strcmp(LocalName1, "NULL")) {
				printf("False\n");
			}
			else {
				printf("True\n");
			}
		}
	}
	return 0;
}

