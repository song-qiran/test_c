#define _CRT_SECURE_NO_WARNINGS 1
//数字金字塔
#include <stdio.h>

void pyramid(int n);

int main()
{
    int n;

    scanf("%d", &n);
    pyramid(n);

    return 0;
}


void pyramid(int n)
{
    int a = 1;//需要打印的数字

     //打印数字前面的空格
    for (; a < n + 1; a++)
    {
        int i = n;
        for (; i - a > 0; i--)
        {
            printf(" ");
        }

        //打印数字
        int b = a;//所打印数字的次数与其大小有关，b是打印次数      
        for (; b > 0; b--)
        {
            printf("%d", a);
            printf(" "); //注意每个数字间以及数字结尾后的空格      
        }
        printf("\n");
    }
}
