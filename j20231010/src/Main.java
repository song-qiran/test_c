/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 * Date: 2023-10-10
 * Time: 10:51
 */
public class Main {
    //8.使用二分查找实现查找数组元素并返回下标
    public static void main(String[] args) {
        int[] arr8 = {1,2,3,4,5};
        int ret8 = binary_sort(arr8,5);
        System.out.println(ret8);
    }
    public  static  int binary_sort(int[] arr8,int key){
        int left = 0;
        int right = arr8.length;
        while(left <= right){
            int m = (left+right)/2;
            if(arr8[m]>key){
                right =m-1;
            } else if (arr8[m]<key) {
                left = m+1;
            }else {
                return  m;
            }
        }
        return -1;
    }





    //7.模拟实现Arrays.toString函数
    public static void main7(String[] args) {
        int [] arr7={1,2,3,4,5};
        my_tostring1(arr7);
        String s1  = my_tostring2(arr7);
        System.out.println(s1);
    }
//实现方法-----直接输出版本
    public  static  void my_tostring1(int[] arr7){
        System.out.print("[");
        for (int i = 0; i < arr7.length-1; i++) {
            System.out.print(arr7[i]+",");
        }
        System.out.print(arr7[arr7.length-1]);
        System.out.print("]");
    }
    //实现方法---转换成字符串输出
    public  static  String my_tostring2(int[] arr7){
        String ret  = "[";
        for (int i = 0; i < arr7.length-1; i++) {
            ret+=arr7[i];
            ret+=",";
        }
        ret+=arr7[arr7.length-1];
        ret+="]";
        return  ret;
    }


    public static void main6(String[] args) {
        int [] arr = {1,2,3,4,5};
        print(arr);
        for (int x:arr) {
            System.out.println();
            System.out.print(x+" ");
        }
    }
    public  static  void print(int [] arr){
        arr = new int []{7,6,5,4};
        for (int x:arr) {
            System.out.print(x+" ");
        }

    }

    //5.求数组的平均值
    //实现一个方法 avg, 以数组为参数, 求数组中所有元素的平均值(注意方法的返回值类型).
    public static  double avg(int[] arr5){
        double sum2 = 0;
        for (int i = 0; i < arr5.length; i++) {
            sum2+=arr5[i];
        }
        return sum2*1.0/arr5.length;
    }
    public static void main5(String[] args) {
        int [] arr5={1,2,3,4,5};
        double ret1 = avg(arr5);
        System.out.println(ret1);
    }


    //4.数组所有元素之和
    //实现一个方法 sum, 以数组为参数, 求数组所有元素之和.
    public  static  void sum(int[] arr4){
        int sum = 0;
        for (int i = 0; i < arr4.length; i++) {
            sum+=arr4[i];
        }
        System.out.println(sum);
    }
    public static void main4(String[] args) {
        int[] arr4 = {1,2,3};
        sum(arr4);
    }

    //3.改变原有数组元素的值
    //实现一个方法 transform, 以数组为参数, 循环将数组中的每个元素
    //乘以 2 , 并设置到对应的数组元素上. 例如 原数组为 {1, 2, 3}, 修改之后为 {2, 4, 6}
    public  static  void transform(int[] arr3){
        for (int i = 0; i < arr3.length; i++) {
            arr3[i]*=2;
        }
        for (int x3: arr3) {
            System.out.println(x3);
        }
    }
    public static void main3(String[] args) {
        int [] arr3 = {1,2,3};
        transform(arr3);
    }



    //2.打印数组
    //实现一个方法 printArray, 以数组为参数, 循环访问数组中的每个元素, 打印每个元素的值.
    public  static  void printArray(int [] arr2){
        for (int x1 : arr2) {
            System.out.println(x1);
        }
    }
    public static void main2(String[] args) {
        int []  arr1 = {1,2,3,4,5};
        printArray(arr1);
    }




    //1.创建数组，并且赋初始值
    //创建一个 int 类型的数组, 元素个数为 100, 并把每个元素依次设置为 1 - 100
    public static void main1(String[] args) {
        int [] arr = new int [100];
        for (int i = 0; i < 100; i++) {
            arr[i] = i+1;
        }
        for (int x : arr ) {
            System.out.print(x+" ");
        }
    }

}