#define _CRT_SECURE_NO_WARNINGS 1
//输出倒三角图案
#include<stdio.h>
int main()
{
    printf("* * * *\n");
    printf(" * * *\n");
    printf("  * *\n");
    printf("   *\n");

    return 0;
}
/*#include<stdio.h>
int main()
{
    int i,j=0;
    for(i=0;i<4;i++)
    {
        for(j=0;j<i;j++)
        {
            printf(" ");
        }
        for(j=0;j<4-i;j++)
        {
            printf("* ");

        }
         printf("\n");
    }
    return 0;
}*/
//  计算分段函数
#include<stdio.h>
int main()
{
    double x = 0;
    scanf("%lf", &x);
    if (x == 0)
        printf("f(0.0) = 0.0");
    else
        printf("f(%.1f) = %.1f", x, 1 / x);
    return 0;
}
