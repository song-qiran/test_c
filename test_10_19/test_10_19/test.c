#define _CRT_SECURE_NO_WARNINGS 1
////高空坠球
//#include <stdio.h>
//#include <math.h>
//int main()
//{
//    int h, n;
//    double sum = 0, height = 0;
//    int i;
//    scanf("%d %d", &h, &n);
//    for (i = 1; i <= n; i++)
//    {
//        if (i == 1)
//            sum += h;
//        else
//            sum += 2 * height;
//        height = h / pow(2, i);
//    }
//    printf("%.1f %.1f\n", sum, height);
//    return 0;
//}


//求分数序列前N项和
#include <stdio.h>
#include <stdlib.h>
#include<math.h>

int main()
{
    int n, i;
    double x, a, fz = 2.0, fm = 1.0, sum = 0.0;
    scanf("%d", &n);
    for (i = 1; i <= n; i++)
    {
        x = fz / fm;
        sum += x;
        a = fm;
        fm = fz;
        fz = fz + a;
    }
    printf("%.2f", sum);
    return 0;
}
