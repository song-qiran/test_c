#define _CRT_SECURE_NO_WARNINGS 1
//1.递归实现n的k次方
//#include<stdio.h>
//#include<stdlib.h> 
//int power(int n, int k)
//{
//	if (k <= 0)
//		return 1;
//	else
//		return n * power(n, k - 1);
//}
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d%d", &n, &k);
//	int ret = power(n, k);
//	printf("%d\n", ret);
//	system("pause");
//	return 0;
//}

//2.计算一个数的每位之和（递归实现）


//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//例如，调用c，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19
//#include<stdio.h>
//int DigitSum(int x)
//{
//	if (x > 9)
//	{
//		return x % 10 + DigitSum(x / 10);
//	}
//	else
//		return x;
//}
//int main()
//{
//	int num = 1729;
//	int ret = DigitSum(num);
//	printf("%d\n", ret);
//	return 0;
//}

//字符串逆序（递归实现）
//#include <stdio.h>
//#include <stdlib.h>
//
//int my_strLen(char str[]);//函数声明
//void reverse_string(char str[]);//函数声明
//
//
//int my_strLen(char str[]) {//需要一个求数组长度的函数 自定义一个my_strLen
//	if (str[0] == '\0') {//当数组开头为结束标志符\0时 表示数组已经遍历完毕
//		return 0;
//	}
//	return 1 + my_strLen(str + 1);//数组长度等于1 + 以第二个元素开头的数组长度
//}
//
////逆序函数
//void reverse_string(char str[]) {
//	int len = my_strLen(str);//首先求出数组长度 长度-1即为数组内最后一个字符下标
//	char tem = *str;//定义一个临时变量储存首字符的内容
//	*str = *(str + len - 1);//将最后一个元素赋值给第一个字符，完成第一组逆序
//	*(str + len - 1) = '\0';//将\0赋值给最后一个字符，使递归找到最后一个字符
//	if (my_strLen(str) > 0) {//如果数组长度不小于0 则一直递归下去
//		reverse_string(str + 1);
//	}
//	*(str + len - 1) = tem;//数组长度小于0，即后半部分已经逆序完毕
//}						//此时将前半部分的值逐个速出即可，就是tem里面存储的值
//
//int main() {
//	char str[] = "abcdefg";
//	printf("before :%s\n", str);
//	reverse_string(str);
//	printf("after :%s\n", str);
//
//	system("pause");
//	return 0;
//}

////strlen的模拟（递归实现）
////递归实现strlen
//#include<stdio.h>
//int Strlen(char* str)
//{
//	if (*str != '\0')
//		return 1 + Strlen(str + 1);
//	else
//		return 0;
//}
//
//int main()
//{
//	char arr[] = "abc";
//	int len = Strlen(arr);
//	printf("%d\n", len);
//
//	return 0;
//}
//非递归实现strlen
//#include <stdio.h>
//int Strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abc";
//	int len = Strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}
//小乐乐最近接触了求和符号Σ，他想计算的结果。
//#include<stdio.h>
//int main()
//{
//    long long n = 0;
//    scanf("%lld", &n);
//    long long sum = 0;
//    sum = (1 + n) * n / 2;
//    printf("%lld\n", sum);
//
//    return 0;
//}

//计算斐波那契数
//递归和非递归分别实现求第n个斐波那契数
//例如：
//输入：5  输出：5
//输入：10， 输出：55
//输入：2， 输出：1
//递归算法
//#include<stdio.h>
//#include<stdlib.h>
//int FeiBo(int f)
//{
//	int n = 0;
//	if (0 == f)
//		printf("0的斐波那契数是0\n");
//	else if (1 == f || 2 == f)
//		n = 1;
//	else n = FeiBo(f - 2) + FeiBo(f - 1);
//	return n;
//}
//int main()
//{
//	int num = 0;
//	int key = 0;
//	scanf("%d", &key);
//	num = FeiBo(key);
//	printf("%d\n", num);
//	system("pause");
//	return 0;
//}
//非递归的方式计算斐波那契数

#include <stdio.h>
#include<stdlib.h>
int fib(int n)
{
	int a1 = 1;
	int a2 = 1;
	int a3 = 0;
	if (n <= 2)
		return 1;
	else
	{
		while (n > 2)
		{
			a3 = a1 + a2;
			a1 = a2;
			a2 = a3;
			n--;
		}
		return a3;
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	printf("%d", fib(n));
	system("pause");
	return 0;
}
