import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 * Date: 2023-10-09
 * Time: 9:34
 */
public class Main {
    //使用foreach实现输出数组元素
    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5};
        for (int x :arr1) {
            System.out.print(x+" ");
        }
    }
    //使用for循环输出数组元素
    public static void main6(String[] args) {
        int [] arr = {1,2,3,4,5};
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }







    //循环方式实现斐波那契数列
    public  static  int fib2(int n){
        if(n == 1 ){
            return 0;
        }
        if(n == 2){
            return 1;
        }
        int f1 = 0;
        int f2 = 1;
        int f3 = -1;
        for (int i = 3; i <= n ; i++) {
            f3 = f1+f2;
            f1 = f2;
            f2 = f3;
        }
        return f3;

    }
//递归方式实现斐波那契数列
public static int fib(int n) {
    if(n == 1 ) {
        return 0;
    }
    if(n == 2) {
        return 1;
    }
    return fib(n-1)+fib(n-2);
}
public static void main5(String[] args) {
    //System.out.println(fib(5));
    System.out.println(fib2(5));
}




    //写递归方法，输入非负整数，返回组成数字的和。
    //输入1729，返回1+7+2+9=19
    public static void main4(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        System.out.println( sum2(num) );
    }
    public  static  int sum2(int num){
        if(num<10){
            return num;
        }
        return sum2(num/10)+num%10;
    }




    //使用递归求1到5的和
    public static void main3(String[] args) {
        int num = 5;
        System.out.println(sum(5));
    }
    public  static  int sum(int num){
        if(num==1){
            return 1;
        }
        return num+sum(num-1);
    }





   //使用递归顺序输出123的每一位
   public static void main2(String[] args) {
       int num = 123;
       print(123);
   }
   public  static void print(int num){
       if(num<10) {
           System.out.println(num);
       }else{
           print(num/10);
           System.out.println(num%10);
       }
   }
   //使用循环输出123的每一位
   public static void main1(String[] args) {
       int num = 123;
       while(num!=0){
           System.out.println(num%10);
           num/=10;
       }
   }
}