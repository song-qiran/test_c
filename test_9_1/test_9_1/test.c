#define _CRT_SECURE_NO_WARNINGS 1
//不用加减乘除做加法
//int Add(int num1, int num2) {
//    int m = 0;
//    int n = 0;
//    m = (num1 & num2) << 1;  // 需要进位
//    n = num1 ^ num2;        //无需进位    此时m + n就是答案，但不能做加法
//    while (n & m)   // 检查是否还需要进位
//    {
//        num1 = m;
//        num2 = n;
//        m = (num1 & num2) << 1;  //需要进位
//        n = num1 ^ num2;        //无需进位
//    }
//
//    return m | n;
//}


//最大连续1的个数
//int findMaxConsecutiveOnes(int* nums, int numsSize)
//{ 
//	int max_count = 0, cur_size = 0;
//	for (int i = 0; i < numsSize; i++)
//{ 
//		if (nums[i] == 1) 
//	{
//			cur_size++;
//		} else 
//		{ max_count = max_count > cur_size ? max_count : cur_size; cur_size = 0;
//		}
//	}
//	max_count = max_count > cur_size ? max_count : cur_size;
//	return max_count;
//}

//完全数计算
#include <stdio.h>
#include <math.h>
int is_perfect_num(int num)
{ 
	int sum = 1;
	for (int i = 2; i <= sqrt(num); i++)
{ 
		if (num % i == 0)
	{//判断是否能够整除i，能整除则i和结果都是约数
			sum += i; //与除数相加
			if (i != sqrt(num))//防止除数和结果相同的情况下重复相加 
				sum += num / i; //与相除结果相加
		} }
	if (sum == num)
		return 1;
	return 0;
}
int main()
{
	int n;
	while (scanf("%d", &n) != EOF)
	{
		int count = 0;
		for (int i = 2; i <= n; i++) {
			//对n以内的数字都进行判断是否是完全数，注意1不参与判断 
			if (is_perfect_num(i)) count++;
		}
		printf("%d\n", count);
		return 0;
	}
}
