#define _CRT_SECURE_NO_WARNINGS 1
//打印100-200之间的素数
//素数——只能被1和他本身整除
//计算素数的方法
//使用2-i-1之间的数字去试除i
#include<stdio.h>
int main()
{
	int i = 0;
	for (i = 100; i <= 200; i++)
	{
		int flag = 1;
		int j = 0;
		for (j = 2; j < i; j++)
		{
			if (i % j == 0)
			{
				flag = 0;
				break;
			}
		}
		if (flag == 1)
		{
			printf("%d ", i);
		}
	}
	return 0;
}