#define _CRT_SECURE_NO_WARNINGS 1

//根据测试样例，其实它是要我们打印第一个字母到第二个字母的所有字符。
//但是真正输出的是第一个字母开始后面的这一串字符，输出为p。
//所以我们仍然采用开关的思路来求解。设置两个开关，t1和t2，一个用于输出
//一个用于打印。当碰到第一个字母的时候开关t1与t2都打开，当碰到第二个字母时开关t2关闭，t1不关闭。
//这样就能够按题目要求打印中间的这段字符，并输出第一个开始往后的一串字符串。

//
//char* match(char* s, char ch1, char ch2)
//{
//    int t1 = 0, t2 = 0, n = 0;
//    int i = 0, j = 0;
//    char* p;
//    p = s;
//    while (*s)
//    {
//        n++;   //统计整个字符数组的个数
//        s++;
//    }
//    for (i = 0; i < n; i++)
//        s--;
//    for (i = 0; i < n; i++)
//    {
//        if (s[i] == ch1 && t2 != 2)
//        {
//            t1 = 1;
//            t2 = 1;
//        }
//        if (t1 == 1)
//        {
//            p[j] = s[i];
//            j++;
//        }
//        if (t2 == 1)
//        {
//            printf("%c", p[j - 1]);
//        }
//        if (s[i] == ch2)
//            t2 = 2;
//        if (i == n - 1)
//            p[j] = '\0';
//
//    }
//    printf("\n");
//    return p;
//}



//字符串逆序
#include<stdio.h>
#include<string.h>
int main()
{
    char str[81] = { 0 };
    int i = 0, j = 0;
    char temp;
    gets(str);
    i = strlen(str);
    char* left = &str[0];
    char* right = &str[i - 1];
    while (left < right)
    {
        temp = *right;
        *right = *left;
        *left = temp;
        left++;
        right--;
    }
    printf("%s", str);
    return 0;
}

//需要注意问题：
//1.字符串数组一定要够长，总数+1，->留出'\0'的位置
//2.打印输出的时候直接%s即可
//3.为避免最后把结尾赋值为'\0'，索性直接把所有数组元素赋值为0
//4.最后元素下标为所有字符串长度-1；