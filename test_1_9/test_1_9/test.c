#define _CRT_SECURE_NO_WARNINGS 1
//�ԳƵĶ�����
public boolean isSymmetric(TreeNode root) {
    return isSameTree(root.left, invert(root.right));
}
public TreeNode invert(TreeNode root) {
    if (root == null)
        return null;
    TreeNode node = root.left;
    root.left = invert(root.right);
    root.right = invert(node);
    return root;
}
public boolean isSameTree(TreeNode p, TreeNode q) {
    if (p == null || q == null)
        return p == q;
    else if (p != null && q != null) {
        if (p.val != q.val)
            return false;
        else if (isSameTree(p.left, q.left) && isSameTree(p.right, q.right))
            return true;
    }
    return false;
}