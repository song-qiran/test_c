#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//练习代码
// int main()
//{
//	int i = 0;
//	while (i < 101)
//	{
//		if (i % 2 == 1)
//		{
//			printf("%d\t", i);
//		}
//		i++;
//	}
//	return 0;
//}
//int func(int a)
//{
//    int b;
//    switch (a)
//    {
//    case 1: b = 30;
//    case 2: b = 20;
//    case 3: b = 16;
//    default: b = 0;
//    }
//    return b;
//}
//int main()
//{
//    return 0;
//}
//#include <stdio.h>
//int main() {
//	int x = 3;
//	int y = 3;
//	switch (x % 2) {
//	case 1:
//		switch (y)
//		{
//		case 0:
//			printf("first");
//		case 1:
//			printf("second");
//			break;
//		default: printf("hello");
//		}
//	case 2:
//		printf("third");
//	}
//	return 0;
//}
//编程一
//  打印3的倍数的数
// 写一个代码打印1 - 100之间所有3的倍数的数字
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 1; i < 100; i++)
//	{
//		if(i % 3 == 0)
//		printf("%d\n", i);
//	}
//	return 0;
//}
//编程二
// 从大到小输出
//
//作业内容
//写代码将三个整数数按从大到小输出。
//
//例如：
//
//输入：2 3 1
//
//输出：3 2 1.
//

//int main()
//{
//	int arr[3] = { 0 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		scanf("%d", &arr[i]); //scanf后面记得看有无空格！！！
//	}
//	for (i = 2; i >= 0; i--)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//
//

//正确解法：
//#include <stdio.h>
//int main() {
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int tmp = 0;
//	scanf("%d %d %d", &a, &b, &c);
//	if (a < b) {
//		tmp = a;
//		a = b;
//		b = tmp;
//	}
//	if (a < c) {
//		tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c) {
//		tmp = b;
//		b = c;
//		c = tmp;
//	}
//	printf("三个整数由大到小依次是：%d %d %d\n", a, b, c);
//	return 0;
//}
//编程三
// 写一个代码：打印100~200之间的素数
//int main()
//{
//	int i = 0;
//	int  j = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//				break;
//		}
//		if (j == i)
//		{
//			
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}
//编程四
//打印1000年到2000年之间的闰年
//闰年：
//普通闰年：公历年份是4的倍数，且不是100的倍数的，为闰年（如2004年、2020年等就是闰年）。
//
//世纪闰年：公历年份是整百数的，必须是400的倍数才是闰年（如1900年不是闰年，2000年是闰年）。
//#include<stdio.h>
//int main()
//{
//    int i = 0;
//    for (i = 1000; i <= 2000; i++)
//    {
//        if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0)
//        {
//            printf("%d 是闰年\n ", i);
//        }
//    }
//    return 0;
//}
//编程五    求最大公约数
//#include <stdio.h>
//int main()
//{
//	int a, b, c;
//	printf("请输入：");
//	scanf("%d%d", &a, &b);
//	if (a < b)
//	{
//		c = a;
//		a = b;
//		b = c;                      
//	}
//	while (b != 0)
//	{
//		c = b;
//		b = a % b; //辗转相除：相比第一种思路大大提高了代码的效率
//		a = c;
//	}
//	printf("最大公约数为：%d", a);
//	return 0;
//}
// 
// 变式：
// 求最小公倍数
// #include<stdio.h>//头文件 
//int main()//主函数 
//{
//    int m, n, num1, num2, temp;//定义整型变量 
//    printf("请输入两个数：");//提示语句 
//    scanf("%d %d", &num1, &num2);//键盘输入两个数 
//    m = num1; //赋值 
//    n = num2; //赋值 
//    while (num2 != 0) // 余数不为0，继续相除，直到余数为0 
//    {
//        temp = num1 % num2;
//        num1 = num2;
//        num2 = temp;
//    }
//    printf("最大公约数是：%d\n", num1);//输出最大公约数 
//    printf("最小公倍数是：%d\n", m * n / num1);//输出最小公倍数 
//}
//
//
//编程六
//在屏幕上输出9 * 9乘法口诀表
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i < 10; i++)
//	{
//		for (j = 1; j < i + 1; j++)
//		{
//			printf("%d * %d = %d  ", j, i, i * j);
//			if (i == j)
//			{
//				printf("\n");
//			}
//		}
//	}
//	return 0;
//}
//编程七
//求10 个整数中最大值
#include <stdio.h>
int main() {
    int max, a[10];
    for (int i = 0; i < 10; i++) {
        scanf("%d", &a[i]);
    }
    max = a[0];
    for (int i = 0; i < 10; i++) {
        max = a[i] > max ? a[i] : max;
    }
    printf("%d\n", max);
    return 0;
}
