#define _CRT_SECURE_NO_WARNINGS 1
//测试代码
//#include<stdio.h>
//int main()
//{
//	int a = 248, b = 4;
//	int const* c = 21;
//	const int* d = &a;
//	int* const e = &b;
//	int const* const f = &a;
//	e = &a;
//
//}

//测试代码
#include <stdio.h>
//int i;
//void prt() 
//{
//	for (i = 5; i < 8; i++)
//	printf("%c", '*');
//	printf("\t");
//}
//int main() 
//{
//	for (i = 5; i <= 8; i++)
//	prt(); 
//	return 0;
//}
//int main() {
//	int a = 3;
//	printf("%d\n", (a += a -= a * a));
//	return 0;
//}

//首先输入要输入的整数个数n，然后输入n个整数。
//输出为n个整数中负数的个数，和所有正整数的平均值，结果保留一位小数。
//0即不是正整数，也不是负数，不计入计算。如果没有正数，则平均值为0。
#include<stdio.h>
int main()
{
    int n = 0;
    int i = 0;
    int a[2000] = { 0 };
    int count = 0;
    double averge = 0;
    int sum = 0;
    int com = 0;
    scanf("%d", &n);//输入整数的个数
    for (i = 0; i < n; i++)
    {
        scanf("%d", &a[i]); //存入每个整数
    }
    for (i = 0; i < n; i++)
    {
        if (a[i] < 0)
        {
            count++;  //统计负整数的个数 
        }
        else if (a[i] > 0)
        {
            com++;
            sum += a[i];
            averge = sum*1.0 / com;
        }

    }
  
    printf("%d %.1lf", count, averge);
    return 0;
}

//旋转数组中的最小数字
//有一个长度为 n 的非降序数组，比如[1,2,3,4,5]
//将它进行旋转，即把一个数组最开始的若干个元素
//搬到数组的末尾，变成一个旋转数组，比如变成了[3,4,5,1,2]，
//或者[4,5,1,2,3]这样的。请问，给定这样一个旋转数组，求数组中的最小值。
//int minNumberInRotateArray(int* rotateArray, int rotateArrayLen)
//{
//    int i = 0;
//    for (i = 1; i < rotateArrayLen; i++)
//    {
//        if (rotateArray[i - 1] > rotateArray[i])
//            return rotateArray[i];
//    }
//    return rotateArray[0];
//}
