#define _CRT_SECURE_NO_WARNINGS 1
//复制字符串->指针
//
//#include <iostream>
//using namespace std;
//
//int main()
//{
//    char str[35] = { 0 };
//    cin.getline(str, sizeof(str));
//    int m;
//    cin >> m;
//    char* p1 = NULL;
//    p1 = &str[m - 1];
//    cout << p1;
//    return 0;
//}

//编写函数实现两数交换（指针方式）
//#include<iostream>
//using namespace std;
//int main()
//{
//    int m, n, tmp;
//    cin >> m >> n;
//    int* p1 = &m, * p2 = &n;
//    tmp = *p1;
//    *p1 = *p2;
//    *p2 = tmp;
//    cout << m << " " << n;
//    return 0;
//}


//利用指针遍历数组
//#include<iostream>
//using namespace std;
//int main()
//{
//    int a[6];
//    int i;
//    for (i = 0; i < 6; i++)
//    {
//        cin >> a[i];
//    }
//    int* p = a;
//    for (i = 0; i < 6; i++)
//    {
//        cout << p[i] << " ";
//    }
//    return 0;
//}

//牛牛的新数组求和
//#include<stdio.h>
//int cal(int* array, int n)
//{
//    int i = 0;
//    int sum = 0;
//    for (i = 0; i < n; i++)
//    {
//        sum += array[i];
//    }
//    return sum;
//}
//int main()
//{
//    int n;
//    scanf("%d", &n);
//    int a[100];
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &a[i]);
//    }
//    printf("%d", cal(a, n));
//    return 0;
//}

//牛牛的排序
//#include<stdio.h>
//void sort(int* array, int n)
//{
//    int i, j;
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < n - i - 1; j++)
//        {
//            if (array[j] > array[j + 1])
//            {
//                int tmp;
//                tmp = array[j];
//                array[j] = array[j + 1];
//                array[j + 1] = tmp;
//            }
//        }
//    }
//    for (i = 0; i < n; i++)
//    {
//        printf("%d ", array[i]);
//    }
//}
//int main()
//{
//    int n;
//    scanf("%d", &n);
//    int a[100];
//    for (int i = 0; i < n; i++)
//        scanf("%d", &a[i]);
//    sort(a, n);
//    return 0;
//}
//
//


