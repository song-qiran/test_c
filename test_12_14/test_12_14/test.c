#define _CRT_SECURE_NO_WARNINGS 1
//顺序表基本操作
//实现顺序表的基本操作，如初始化、插入、删除、输出等。
bool ListInsert(SqList*& L, int i, ElemType e) {
    if (i <= 0 || i > L->length + 1 || L->length == MaxSize)
        return false;
    int j;
    i--;
    if (i == L->length) {
        L->data[i] = e;
        L->length++;
    }
    else {
        for (j = L->length; j > i; j--)
            L->data[j] = L->data[j - 1];
        L->data[i] = e;
        L->length++;
        e = L->data[i - 1];
    }
    return true;
}

bool ListDelete(SqList*& L, int i, ElemType& e) {
    if (i < 1 || i > L->length)
        return false;
    i--;
    e = L->data[i];
    int k;
    for (k = i; k < L->length; k++)
        L->data[k] = L->data[k + 1];
    L->length--;
    return true;
}

bool ListDeleteElem(SqList*& L, ElemType e) {
    int i = 0, j = 0, f = 0;
    while (i + j < L->length) {
        L->data[i] = L->data[i + j];
        if (L->data[i] == e)j++, f = 1;
        else
            i++;
    }
    L->length = L->length - j;
    if (f == 0)return false;
    return true;
}
#include <stdio.h>
#include <malloc.h>
#define MaxSize 10000
typedef char ElemType;
typedef struct
{
    ElemType data[MaxSize];
    int length;
} SqList;
void InitList(SqList*& L);    //初始化线性表，裁判程序实现，略去不表。
void DestroyList(SqList*& L);        //销毁线性表，裁判程序实现，略去不表。
void DispList(SqList* L);  //顺序输出顺序表元素值。每个结点元素值以空格符间隔，裁判程序实现，略去不表。
bool ListInsert(SqList*& L, int i, ElemType e);  //在第i位上插入一个元素e，插入成功时返回true,插入不成功时返回false. 其中：i为顺序表的逻辑序号。
bool ListDelete(SqList*& L, int i, ElemType& e); //在第i个结点上删除一个元素, 返回删除的元素值e,且删除成功时返回true，删除不成功返回false。其中：i为顺序表的逻辑序号。
bool ListDeleteElem(SqList*& L, ElemType e);  //删除元素值为e的结点，删除成功时返回true，删除 不成功返回false。

int main()
{
    SqList* L;
    ElemType e, ch;
    int i = 1;
    InitList(L);
    while ((ch = getchar()) != '\n')
    {
        ListInsert(L, i, ch);  //在L的第i个结点位置上插入ch
        i++;
    }
    DispList(L);
    scanf("\n%d %c", &i, &ch);
    ListInsert(L, i, ch);
    DispList(L);
    scanf("\n%d", &i);
    if (ListDelete(L, i, e)) // 删除L的第i个结点
    {
        printf("delete %dth: %c\n", i, e);
        DispList(L);
        if (ListDeleteElem(L, e))       // 删除元素值为e的结点
            printf("delete \'%c\' \n", e);
        else
            printf("delete \'%c\' failed!\n", e);
    }
    else
        printf("delete %dth failed!\n", i);
    DispList(L);
    DestroyList(L);
}

void InitList(SqList*& L)    //裁判程序实现，略去不表。
{
    ...
}
void DestroyList(SqList*& L) //裁判程序实现，略去不表。
{
    ...
}
void DispList(SqList* L) //裁判程序实现，略去不表。
{
    ...
}

//你的代码将被嵌在这里
