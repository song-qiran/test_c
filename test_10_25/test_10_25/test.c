#define _CRT_SECURE_NO_WARNINGS 1
//简单代码检验switch case 穿越性
//#include<stdio.h>
// int getValue(int i) {
//    int result = 0;
//    switch (i) {
//    case 1:
//        result = result + i;
//    case 2:
//        result = result + i * 2;
//    case 3:
//        result = result + i * 3;
//    }
//    return result;
//}
//int main()
//{
//    printf("%d",getValue(2));
//}


//简化的插入排序
#include<stdio.h>
int main()
{
	int n, i, t, x[100], flag = 1;		//flag=1，flag作判断的标志 

	scanf("%d", &n);            			//需要输入n个数 
	for (i = 0; i < n; i++)					//输入n个数 
		scanf("%d", &x[i]);
	scanf("%d", &t);						//输入需要插入的数 

	for (i = 0; i < n; i++)					//从x[0]开始依次比较 
	{
		if (t <= x[i] && flag)				//判断需要插入的数 t 是否≤x[i]，且flag==1 
		{										//因为	t > x[i-1]	，且flag==1 时，才可能运行到这里 
			printf("%d ", t);				//若是，输出t	->	此时已完成插入的工作 
			flag = 0;							//让flag=0 
		}										//从此这个if不会再执行 
		printf("%d ", x[i]);				//从此，通过for循环，依次输出 t 后面的数 
	}											//					即从x[i]开始依次输出 
	if (t >= x[n - 1]) printf("%d ", t);	//如果t比最后一个数大，则最后输出t

	return 0;
}
