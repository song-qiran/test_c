#define _CRT_SECURE_NO_WARNINGS 1
//求整数序列中出现次数最多的数
//#include<stdio.h>
//int main()
//{
//    int n;
//    scanf("%d", &n);
//    int a[1000];
//    int count = 0;
//    int max = 0, maxcount = 0;
//    int i, j;
//    for (i = 0; i < n; i++)//将数字填入数组
//    {
//        scanf("%d", &a[i]);
//    }
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < n; j++)
//        {
//            if (a[i] == a[j])//计算i的次数
//            {
//                count++;
//            }
//        }
//        if (count > maxcount)//比较i的次数
//        {
//            maxcount = count;
//            max = a[i];
//        }
//        count = 0;//这里一定要清零，否则会记录所有元素的次数，我老是忘记这点
//    }
//    printf("%d %d", max, maxcount);
//    return 0;
//}


//求最大值及其下标
#include <stdio.h>

int main()
{
	int a[10], i, n, max, index;
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		scanf("%d", &a[i]);
	}
	max = a[0];
	for (i = 0; i < n; i++) {
		if (max < a[i]) {
			max = a[i];
			index = i;
		}
	}
	printf("%d %d", max, index);
	return 0;
}
