#define _CRT_SECURE_NO_WARNINGS 1
//KiKi想获得某年某月有多少天，请帮他编程实现。输入年份和月份，计算这一年这个月有多少天。
//#include<stdio.h>
//int main()
//{
//    int rarr[12] = { 31,29,31,30,31,30,31,31,30,31,30,31 };
//    int parr[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//    int year = 0;
//    int month = 0;
//    while ((scanf("%d %d", &year, &month)) != EOF)
//    {
//        if (year % 400 == 0 || year % 4 == 0 && year % 100 != 0)
//        {
//            printf("%d\n", rarr[month - 1]);
//        }
//        else
//            printf("%d\n", parr[month - 1]);
//    }
//    return 0;
//}

//在现OJ题
//KiKi想判断输入的字符是不是字母，请帮他编程实现。
//
//输入描述：
//多组输入，每一行输入一个字符。
//输出描述：
//针对每组输入，输出单独占一行，判断输入字符是否为字母，输出内容详见输出样例
#include<stdio.h>
int main()
{
    char c = 0;
    while ((c = getchar()) != EOF)
    {
        if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z')
        {
            printf("%c is an alphabet.\n", c);
        }
        else
        {
            printf("%c is not an alphabet.\n", c);
        }
        getchar();
    }
    return 0;
}
//注意：
//遇到单个输入字符的问题要使用getchar（）吸收回车键