#define _CRT_SECURE_NO_WARNINGS 1
//整数的个位数字
#include<stdio.h>
int main()
{
    int a = 0;
    scanf("%d", &a);
    printf("%d", a % 10);
    return 0;
}

//整数的十位数字
#include<stdio.h>
int main()
{
    int a = 0;
    scanf("%d", &a);
    printf("%d", a / 10 % 10);
    return 0;
}

//原计划星期X开学，通知开学时间延期N天，请问开学日期是星期几
#include<stdio.h>
int main()
{
    int x, n, y = 0;
    scanf("%d %d", &x, &n);
    y = x + n;
    while (y > 7)
    {
        y -= 7;
    }
    if (y <= 7)
    {
        printf("%d", y);
    }
    return 0;
}