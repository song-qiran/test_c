#define _CRT_SECURE_NO_WARNINGS 1
//获取数组最大值的四种方式
//1.循环对比法
int max = arr[0];
for (int i = 0; i < arr.length; i++) {
    if (arr[i] > max) {
        max = arr[i];
    }
}
//2.Arrays.sout
Arrays.sort(arr);
int max = arr[arr.length - 1];
//3.Arrays.stream方法
public static void main(String[] args) {
    int[] arr = { 53,3,542,748,14,214 };
    int max = findMaxByStream(arr); // 根据 stream 查找最大值
    System.out.println("最大值是：" + max);
}


/**
 * 根据 stream 查找最大值
 * @param arr 待查询数组
 * @return 最大值
 */
private static int findMaxByStream(int[] arr) {
    return Arrays.stream(arr).max().getAsInt();
}
//4.Collections.max方法

int[] arr = { 53,3,542,748,14,214 };
List list = Arrays.asList(arr);