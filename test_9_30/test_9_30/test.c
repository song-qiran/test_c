#define _CRT_SECURE_NO_WARNINGS 1
//6 - 2 简单实现x的n次方
//double mypow(double x, int n)
//{
//    int i = 0;
//    double tmp = 1;
//    if (n == 0)
//    {
//        return 1;
//    }
//    else {
//        for (i = 1; i <= n; i++)
//        {
//            tmp *= x;
//        }
//        return tmp;
//    }
//
//}


//使用函数求1到10的阶乘和
//
//double fact(int n)
//{
//    int i = 0;
//    double sum = 1;
//    for (i = 1; i <= n; i++)
//    {
//        sum *= i;
//    }
//    return sum;
//}
//#include <stdio.h>
//
//double fact(int n);
//
//int main(void)
//{
//    int i;
//    double sum;
//
//    sum = 0;
//    for (i = 1; i <= 10; i++)
//        sum = sum + fact(i);
//
//    printf("1!+2!+...+10! = %f\n", sum);
//    return 0;
//}
//
///* 你的代码将被嵌在这里 */
//

////6 - 4 使用函数计算两点间的距离
//double dist(double x1, double y1, double x2, double y2)
//{
//    double sum = 0;
//    sum = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
//    return sum;
//}
//
//#include <stdio.h>
//#include <math.h>
//
//double dist(double x1, double y1, double x2, double y2);
//
//int main()
//{
//    double x1, y1, x2, y2;
//
//    scanf("%lf %lf %lf %lf", &x1, &y1, &x2, &y2);
//    printf("dist = %.2f\n", dist(x1, y1, x2, y2));
//
//    return 0;
//}
//


//
//6 - 5 符号函数
//int sign(int x)
//{
//    if (x > 0)
//        return 1;
//    if (x == 0)
//        return 0;
//    else
//        return -1;
//}
//#include <stdio.h>
//
//int sign(int x);
//
//int main()
//{
//    int x;
//
//    scanf("%d", &x);
//    printf("sign(%d) = %d\n", x, sign(x));
//
//    return 0;
//}
//


