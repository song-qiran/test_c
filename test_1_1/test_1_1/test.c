#define _CRT_SECURE_NO_WARNINGS 1
//哈夫曼编码
/*在ht[1]至ht[n]的范围内选择两个parent为0且weight最小的结点，其序号分别赋给s1，s2*/
void Select(HuffmanTree ht, int n, int* s1, int* s2) {
	int i, min1 = MAX, min2 = MAX;
	*s1 = 0;
	*s2 = 0;
	for (i = 1; i <= n; i++) {
		if (ht[i].parent == 0) {
			if (ht[i].weight < min1) {
				min2 = min1;
				*s2 = *s1;
				min1 = ht[i].weight;
				*s1 = i;
			}
			else if (ht[i].weight < min2) {
				min2 = ht[i].weight;
				*s2 = i;
			}
		}
	}
}

/*创建哈夫曼树算法*/
void CrtHuffmanTree(HuffmanTree ht, int w[], int n) {
	//构造哈夫曼树ht[M+1],w[]存放n个权值
	int i;
	for (i = 1; i <= n; i++) {		//1至n号单元存放叶子结点，初始化
		ht[i].weight = w[i - 1];
		ht[i].parent = 0;
		ht[i].LChild = 0;
		ht[i].RChild = 0;
	}
	int m = 2 * n - 1;				//所有结点总数
	for (i = n + 1; i <= m; i++) {	//n+1至m号单元存放非叶结点，初始化
		ht[i].weight = 0;
		ht[i].parent = 0;
		ht[i].LChild = 0;
		ht[i].RChild = 0;
	}

	/*初始化完毕，开始创建非叶结点*/
	int s1, s2;
	for (i = n + 1; i <= m; i++) {	//创建非叶结点，建哈夫曼树
		Select(ht, i - 1, &s1, &s2);//在ht[1]至ht[i-1]的范围内选择两个parent为0且weight最小的结点，其序号分别赋给s1，s2
		ht[i].weight = ht[s1].weight + ht[s2].weight;
		ht[s1].parent = i;
		ht[s2].parent = i;
		ht[i].LChild = s1;
		ht[i].RChild = s2;
	}
}
