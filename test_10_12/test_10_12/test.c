#define _CRT_SECURE_NO_WARNINGS 1
//计算火车运行时间
//#include<stdio.h>
//int main()
//{
//    int start, end = 0;
//    int a, b, c, d = 0;
//    scanf("%d %d", &start, &end);
//    a = start / 100;   //12
//    c = end / 100;      //15
//    b = start - a * 100;  //1
//    d = end - c * 100;    //30
//    a = c - a;              //3
//    c = d - b;          //29
//    if (c < 0)                  //num1，num2都是在同一天,h一定大于0
//    {
//        c = 60 + c;                 //当m小于0，从h借来一小时，填补m的值
//        a = a - 1;
//    }
//    printf("%02d:%02d", a, c);
//    return 0;
//}
//
////求N分之一序列前N项和
//#include<stdio.h>
//int main()
//{
//    int i, n = 0;
//    double sum = 0;
//    scanf("%d", &n);
//    for (i = 1; i <= n; i++)
//    {
//        sum += 1.0 / i;
//    }
//    printf("sum = %.6lf", sum);
//    return 0;
//}


//求奇数分之一序列前N项和   //要注意i的最大值的范围
//#include<stdio.h>
//int main()
//{
//    int i, n = 0;
//    double sum = 0;
//    scanf("%d", &n);
//    for (i = 1; i <= 2 * n - 1; i += 2)
//    {
//        sum += 1.0 / i;
//    }
//    printf("sum = %.6lf", sum);
//    return 0;
//}
//
////求简单交错序列前N项和
//#include<stdio.h>
//int main()
//{
//    int i, n = 0;
//    double sum = 0;
//    scanf("%d", &n);
//    for (i = 1; i <= 3 * n - 2; i += 3)
//    {
//        if (i % 2 != 0)
//            sum += 1.0 / i;
//        else
//            sum -= 1.0 / i;
//
//    }
//    printf("sum = %.3lf", sum);
//    return 0;
//}   



//求平方与倒数序列的部分和
#include<stdio.h>
int main()
{
    int m = 0;
    int n = 0;
    double sum = 0;
    scanf("%d %d", &m, &n);
    while (m <= n)
    {
        sum += m * m + 1.0 / m;
        m++;
    }
    printf("sum = %.6lf\n", sum);
    return 0;
}