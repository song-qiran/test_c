package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 * Date: 2023-10-18
 * Time: 15:52
 */
interface Iswimming{
    void swim();
}
interface Irunning{
    void run();
}
interface Iflying{
    void  fly();
}
abstract  class Animal{
    private String name;
    private  int age;
    Animal(String name,int age){
        this.name=name;
        this.age=age;
    }

    public abstract void eat();

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
class Dog extends Animal implements Irunning,Iswimming{
    Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName()+"吃狗粮");
    }
    @Override
    public void swim() {
        System.out.println(this.getName()+"在狗刨");
    }

    @Override
    public void run() {
        System.out.println(this.getName()+"在跑步");
    }
}
class Bird extends Animal implements Iflying,Iswimming{
    Bird(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName()+"吃鸟粮");
    }

    @Override
    public void fly() {
        System.out.println(this.getName()+"在飞");
    }
    @Override
    public void swim() {
        System.out.println(this.getName()+"在游泳");
    }

}
public class demo2{
    public static void test1(Animal animal) {
        animal.eat();
    }
    public static void test2(Iflying iflying) {
        iflying.fly();
    }
    public static void test3(Irunning irunning) {
        irunning.run();
    }
    public static void test4(Iswimming iswimming) {
        iswimming.swim();
    }
    public static void main(String[] args) {
        test1(new Dog("xiaogou",2));
    }
}
