package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: songq
 * Date: 2023-10-18
 * Time: 15:05
 */
//实现多态1---吃东西
//    class animal {
//    public void eat() {
//        System.out.println("吃饭");
//    }
//}
//    class dog extends  animal {
//        @Override
//        public void eat() {
//            System.out.println("吃狗粮");
//        }
//    }
//        class cat extends  animal{
//            public  void eat(){
//                System.out.println("吃猫粮");
//            }
//    }



    //多态2 画图形
    class square{
        public void draw(){
            System.out.println("画图形");
        }
}
    class circle extends square{
        @Override
        public void draw() {
            System.out.println("圆形");
        }
    }
    class trc extends  square{
        @Override
        public void draw() {
            System.out.println("画椭圆");
        }
    }
public class demo1 {
        public static  void data(square sq){
            sq.draw();
        }
    public static void main(String[] args) {
        trc t = new trc();
        circle c = new circle();
        data(t);
        data(c);
    }
}
