#define _CRT_SECURE_NO_WARNINGS 1
//编程一
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//（一）使用非递归
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    int i = 1;
//    int j = 1;
//    printf("请输入n：");
//    scanf("%d", &n);
//    for (; i <= n; i++) {
//        j = j * i;
//    }
//    printf("阶乘为：%d", j);
//    return 0;
//}
//（二）使用递归
//#include <stdio.h>
//int func(int n)
//{
//    if (n <= 1)
//        return 1;
//    else
//        return n * func(n - 1);
//}
//int main()
//{
//    int n = 1;
//    printf("请输入n:");
//    scanf("%d", &n);
//    printf("阶乘为：%d", func(n));
//    return 0;
//}
//编程二
//递归方式实现打印一个整数的每一位
#include<stdio.h>
//循环得几位函数
int JiWei(int x)
{
	int i, j;
	//x/i==0会break
	for (i = 10, j = 1; x / i; i *= 10, j++);
	//打印数有几位
	printf("它是%d位的。\n", j);
	//返回几位
	return j;
}
//递归打印数的每一位函数
void Print(int x, int jiwei)
{
	//条件判定数修改——防止死递归
	jiwei--;
	//递归
	if (jiwei)
		Print(x / 10, jiwei);
	//打印数的每一位
	printf("数的每%d位:", jiwei + 1);
	printf("%d\n", x % 10);
}
//主函数
int main()
{
	//定义数
	int number;
	//输入数值
	printf("输入数值:");
	scanf("%d", &number);
	//打印数每一位
	Print(number, JiWei(number));
	return 0;
}


