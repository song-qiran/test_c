#define _CRT_SECURE_NO_WARNINGS 1
//递归方式实现打印一个整数的每一位
#include<stdio.h>
int Weishu(int x)
{
	int i, j;
	for (i = 10, j = 1; x / i; i *= 10, j++);
	printf("它是%d位的。\n", j);
	return j;
}
void Print(int x, int jiwei)
{

	jiwei--;
	if (jiwei)
		Print(x / 10, jiwei);
	printf("数的每%d位:", jiwei + 1);
	printf("%d\n", x % 10);
}
int main()
{
	int number;
	printf("输入数值:");
	scanf("%d", &number);
	Print(number, Weishu(number));
	return 0;
}

//使用递归方式求阶乘
#include <stdio.h>
int func(int n)
{
	if (n <= 1)
		return 1;
	elsereturn n* func(n - 1);
}
int main()
{
	int n = 1;
	printf("请输入n:");
	scanf("%d", &n);
	printf("阶乘为：%d", func(n));
	return 0;
}