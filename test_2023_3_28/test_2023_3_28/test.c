#define _CRT_SECURE_NO_WARNINGS 1
//第一种 使用递归
/*#include<stdio.h>
int fibonacci(int x) {
    if (x == 0 || x == 1)
        return x;
    else {
        return fibonacci(x - 1) + fibonacci(x - 2);
    }
}
int main() {
    int x;
    scanf("%d", &x);
    printf("%d", fibonacci(x));
    return 0;
}*/
//第二种 优化时间复杂度
/*#include<stdio.h>
int fibonacci(int x)
{
    int i;
    int a[10000];
    a[0]=0;
    a[1]=1;
    for(i=2;i<=x;i++)
    {
        a[i]=a[i-1]+a[i-2];
    }
    return a[x];
}
int main()
{
    int x;
    scanf("%d",&x);
    printf("%d",fibonacci(x));
    return 0;
}*/
//第三种优化空间复杂度
#include<stdio.h>
int fibonacci(int x)
{
    if (x == 0 || x == 1)
        return x;
    int f1 = 0;
    int f2 = 1;
    int i, f;
    for (i = 2; i <= x; i++)
    {
        f = f1 + f2;
        f1 = f2;
        f2 = f;
    }
    return f;

}
int main()
{
    int x;
    scanf("%d", &x);
    printf("%d", fibonacci(x));
    return 0;
}
















