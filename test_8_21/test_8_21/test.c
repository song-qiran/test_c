#define _CRT_SECURE_NO_WARNINGS 1
//测试代码
//#include<stdio.h>
//int main() 
//{
//char s[] = "\\123456\123456\t";
//printf("%d\n", strlen(s));
//return 0; 
//}
// 测试代码
//#include <stdio.h>
//#define N 2 
//#define M N + 1
//#define NUM (M + 1) * M / 2 
//int main()
//{	printf("%d\n", NUM);
//	return 0; 
//}
//测试代码
//#include<stdio.h>
//int f(int n) 
//{
//	static int i = 1; 
//	if (n >= 5)
//	return n;
//	n = n + i;
//	i++;
//	return f(n); 
//}
//int main()
//{
//	int k = f(1);
//	printf("%d", k);
//}
//
//证明尼科彻斯定理
//验证尼科彻斯定理，即：任何一个整数m的立方都可以写成m个连续奇数之和。
//
//例如：
//
//1 ^ 3 = 1
//
//2 ^ 3 = 3 + 5
//
//3 ^ 3 = 7 + 9 + 11
//
//4 ^ 3 = 13 + 15 + 17 + 19
//#include <stdio.h>
//int main()
//{
//    int n;
//    while (scanf("%d", &n) != EOF) {
//        int start = n * n - n + 1;
//        printf("%d", start);
//        int i;
//        for (i = 1; i < n; i++) {
//            printf("+%d", start + 2 * i);
//        }
//
//        printf("\n");
//    }
//
//    return 0;
//}

//等差数列 2，5，8，11，14。。。。
//（从 2 开始的 3 为公差的等差数列）
//输出求等差数列前n项和
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int sum = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        sum = n * (3 * n + 1) / 2;
//        printf("%d", sum);
//    }
//    return 0;
//}
//或者遍历等差数列进行求和
#include<stdio.h>

int fun(int n)
{
    return 3 * n - 1;
}

int main()
{
    int n;

    while (EOF != scanf("%d", &n))
    {
        int sum = 0;
        for (int i = 1; i <= n; i++)
        {
            sum += fun(i);
        }
        printf("%d\n", sum);
    }

    return 0;
}
